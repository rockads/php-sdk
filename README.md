# Suite's SDK

## Whats is Suite?
Suite is a SSO (single sign on) server with centralizing Teknasyon services like applications, customers and users in one place to access all of them from its control panel.

<img src="examples/assets/sso.jpeg" alt="suite login page" style="display: block;  width: 40%; margin-left: auto; margin-right: auto;"/>


## Installation

Get package Via Composer:

``` bash
$ composer require rockads/suite
```

Merge package config with your system's config:

* Laravel:

``` bash
$ php artisan vendor:publish --provider="Rockads\Suite\SuiteServiceProvider" --tag="config"
```
  
* None Laravel:

   1. Just copy or merge [config](/config/suite.php) file with your service's main config file.
   2. Solve dependency injection for two main class of SDK in your service container.( [OAuth](/src/OAuth.php) & [Suite](/src/Suite.php) )

## Usage
After installation follow these below links to understand workflow and how to integration:


*  [OAuth](/examples/oAuth.md)
*  [Profile](/examples/profile.md)
*  [Users](/examples/user.md)
*  [Services](/examples/service.md)
*  [Applications](/examples/application.md)

For more information about Suite's API, you can visit [here](https://id.rockads.com/api/v1/documentation).

## Test

``` bash
$ composer test
```

for running composer on docker without installing php use this command:

``` bash
$ docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer <command>
```

## Change log

Please see the [changelog](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email amirshadanfar@teknasyon.com instead of using the issue tracker.

## Credits

- [Mutlu Suat Özsarı](https://teknasyon.slack.com/team/U08L1DKHS)
- [Amir Shadanfar](https://teknasyon.slack.com/team/U020PL7NENP)
- [Murat Topuz](https://teknasyon.slack.com/team/U02NU9JR8CR)