<?php

return [
    'base_url' => $_ENV['SUITE_BASE_URL'] ?? \Rockads\Suite\Constants\ServerDomain::STAGE,
    'client_id' => $_ENV['SUITE_CLIENT_ID'] ?? '',
    'client_secret' => $_ENV['SUITE_CLIENT_SECRET'] ?? '',
    // your system redirect url(where redirect user to Suite's login page)
    'oauth_redirect_uri' => $_ENV['SUITE_OAUTH_REDIRECT_URI'] ?? 'http://your-domain/oauth/redirect',
    // Define a url which Suite send exchange code there(should be same on Suite's client database)
    'oauth_callback_uri' => $_ENV['SUITE_OAUTH_CALLBACK_URI'] ?? 'http://your-domain/oauth/callback',
];
