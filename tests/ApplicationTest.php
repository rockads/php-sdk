<?php

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Rockads\Suite\Constants\PlatformType;
use Rockads\Suite\Constants\ResultType;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\Suite;
use Faker\Factory;
use Faker\Generator;

class ApplicationTest extends TestCase
{

    protected GuzzleHttp\Client $client;

    protected GuzzleHttp\Handler\MockHandler $mock;

    protected Token $token;

    protected Generator $faker;

    protected Suite $suite;

    public function setUp(): void
    {
        $this->faker = Factory::create();
        // token
        $this->token = new Token([
            "token_type" => "Bearer",
            "expires_in" => $this->faker->unixTime,
            "access_token" => $this->faker->uuid,
            "refresh_token" => $this->faker->uuid,
        ]);
        // config
        $config = Config::make([
            'base_url' => 'https://id.rockads.com',
            'client_id' => $this->faker->numberBetween(1, 10),
            'client_secret' => $this->faker->uuid,
            'oauth_redirect_uri' => sprintf('%s/oauth/redirect', $this->faker->domainName),
            'oauth_callback_uri' => sprintf('%s/oauth/callback', $this->faker->domainName),
        ]);
        // guzzle
        $mock = new MockHandler();
        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);
        $this->mock = $mock;
        // Suite
        $this->suite = new Suite($this->token, $config, $this->client);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function tesAll()
    {
        // mock response
        $returnList = '{"data":[{"id":"73785312941178880","name":"get contact 2 updated","icon":null,"customer_id":1,"created_at":"2022-03-23 14:36:29","updated_at":"2022-03-23 14:45:22","platforms":[{"id":"73787548299689984","platform":"ios","bundle_id":"111","created_at":"2022-03-23 14:45:22","updated_at":"2022-03-23 14:45:22"},{"id":"73787548446490624","platform":"android","bundle_id":"222","created_at":"2022-03-23 14:45:22","updated_at":"2022-03-23 14:45:22"}],"customer":{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37"}},{"id":"73784328231845888","name":"get contact","icon":null,"customer_id":1,"created_at":"2022-03-23 14:32:34","updated_at":"2022-03-23 14:32:34","platforms":[],"customer":{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37"}}],"status":true,"message":"Applications retrieved successfully."}';
        $returnPaginate = '{"data":[{"id":"73785312941178880","name":"get contact 2 updated","icon":null,"customer_id":1,"created_at":"2022-03-23 14:36:29","updated_at":"2022-03-23 14:45:22","platforms":[{"id":"73787548299689984","platform":"ios","bundle_id":"111","created_at":"2022-03-23 14:45:22","updated_at":"2022-03-23 14:45:22"},{"id":"73787548446490624","platform":"android","bundle_id":"222","created_at":"2022-03-23 14:45:22","updated_at":"2022-03-23 14:45:22"}],"customer":{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37"}},{"id":"73784328231845888","name":"get contact","icon":null,"customer_id":1,"created_at":"2022-03-23 14:32:34","updated_at":"2022-03-23 14:32:34","platforms":[],"customer":{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37"}}],"links":{"first":"http://localhost/api/v1/applications/paginate?page=1","last":"http://localhost/api/v1/applications/paginate?page=54","prev":null,"next":"http://localhost/api/v1/applications/paginate?page=2"},"meta":{"current_page":1,"from":1,"last_page":54,"links":[{"url":null,"label":"&laquo; Previous","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=1","label":"1","active":true},{"url":"http://localhost/api/v1/applications/paginate?page=2","label":"2","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=3","label":"3","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=4","label":"4","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=5","label":"5","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=6","label":"6","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=7","label":"7","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=8","label":"8","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=9","label":"9","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=10","label":"10","active":false},{"url":null,"label":"...","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=53","label":"53","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=54","label":"54","active":false},{"url":"http://localhost/api/v1/applications/paginate?page=2","label":"Next &raquo;","active":false}],"path":"http://localhost/api/v1/applications/paginate","per_page":"2","to":2,"total":108},"status":true,"message":"Applications retrieved successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnList));
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnPaginate));
        // get class instance & run method
        $responseList = $this->suite->application()->all(ResultType::LIST);
        $responsePaginate = $this->suite->application()->all(ResultType::PAGINATE);
        // assertions
        $this->assertSame($responseList, json_decode($returnList, true));
        $this->assertSame($responsePaginate, json_decode($returnPaginate, true));
    }

    public function testCreate()
    {
        // mock response
        $return = '{"data":{"id":"83506657081950208","name":"app-test-1","icon":"applications_icon/AZwxtIPD0K0mLTUfr3pStTzgkDBT6N8v29KggirI.png","customer_id":1,"created_at":"2022-04-19 10:25:38","updated_at":"2022-04-19 10:25:38","platforms":[],"customer":{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37"}},"status":true,"message":"Application created successfully."}';
        $returnWithPlatform = '{"data":{"id":"83507488774684672","name":"app-test-1","icon":null,"customer_id":1,"created_at":"2022-04-19 10:28:56","updated_at":"2022-04-19 10:28:56","platforms":[{"id":"83507488900513792","platform":"ios","bundle_id":"1","created_at":"2022-04-19 10:28:56","updated_at":"2022-04-19 10:28:56"},{"id":"83507488976011264","platform":"android","bundle_id":"2","created_at":"2022-04-19 10:28:56","updated_at":"2022-04-19 10:28:56"}],"customer":{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37"}},"status":true,"message":"Application created successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnWithPlatform));
        // run method
        $response = $this->suite->application()->create($this->faker->name, [], $this->faker->imageUrl);
        $responseWithPlatform = $this->suite->application()->create($this->faker->name, [
            ['platform' => $this->faker->randomElement(PlatformType::toArray()), 'bundle_id' => $this->faker->randomDigit()],
            ['platform' => $this->faker->randomElement(PlatformType::toArray()), 'bundle_id' => $this->faker->randomDigit()]
        ]);
        // assertions
        $this->assertSame($response, json_decode($return, true));
        $this->assertSame($responseWithPlatform, json_decode($returnWithPlatform, true));
    }

    public function testUpdate()
    {
        // mock response
        $return = '{"data":{"id":"82062276055531520","name":"","username":null,"email":"asdasd@teknasyon.com","phone":null,"language":"tr","timezone":"UTC","last_login_date":null,"last_login_workspace_id":"2","avatar":null,"email_verified_at":null,"should_reset_password":true,"created_at":"2022-04-15 10:46:11","updated_at":"2022-04-15 10:46:11","services":[{"id":"1","name":"Marketing","access_status":true,"service_role_id":"101","link":"https://test-web.rockads.com/login","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"}],"customers":[{"id":"2","name":"Customer One","workspace":"customer_one","status":"pending","is_owner":0,"who_invited_id":37853166263336960,"invited_accept_at":null,"logged_in":false,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370545315840","name":"Member","customer_id":"2","created_at":"2021-10-28 14:00:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]},"status":true,"message":"User created successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // run method
        $response = $this->suite->application()->update(82062276055531520, $this->faker->name, [['platform' => $this->faker->randomElement(PlatformType::toArray()), 'bundle_id' => $this->faker->randomDigit()]]);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testShow()
    {
        // mock response
        $return = '{"data":{"id":"83506657081950208","name":"app-test-1","icon":"applications_icon/AZwxtIPD0K0mLTUfr3pStTzgkDBT6N8v29KggirI.png","customer_id":1,"created_at":"2022-04-19 10:25:38","updated_at":"2022-04-19 10:25:38","platforms":[],"customer":{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37"}},"status":true,"message":"Application created successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // run method
        $response = $this->suite->application()->show(83506657081950208);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testDelete()
    {
        // mock response
        $return = '{"status":true,"message":"Application deleted successfully.","data":[]}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->application()->destroy(83506657081950208);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testGeneratePresignedUrl()
    {
        // mock response
        $return = '{"status":true,"message":"Temporary upload url has generated.","data":{"url":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/applications_icon/31428899871129600/image-name?x-amz-acl=public-read&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA3EBABPYQIE7RSL74%2F20220419%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Date=20220419T080509Z&X-Amz-SignedHeaders=host%3Bx-amz-acl&X-Amz-Expires=600&X-Amz-Signature=9de7247a412927f8d70547df249eea54f23619a72146a3b86d692ba177b18c99"}}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->application()->generatePresignedUrl(83506657081950208, $this->faker->name);
        // assertions
        $this->assertSame($response, json_decode($return, true)['data']['url']);
    }

    public function testGetCreatePageUrl()
    {
        $config = Config::getInstance();
        $continue = $this->faker->domainName . '/application/list';
        // mock response
        $return = sprintf('{"status":true,"message":"Retrieved application create url successfully.","data":"%s/apps/create"}', $config->getBaseUrl());
        $returnWithContinue = sprintf('{"status":true,"message":"Retrieved application list url successfully.","data":"%s/apps/create?continue=%s"}', $config->getBaseUrl(), $continue);
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnWithContinue));
        // get class instance & run method
        $response = $this->suite->application()->getCreatePageUrl();
        $responseWithContinue = $this->suite->application()->getCreatePageUrl($continue);
        // assertions
        $this->assertSame($response, json_decode($return, true)['data']);
        $this->assertSame($responseWithContinue, json_decode($returnWithContinue, true)['data']);
    }

    public function testGetListPageUrl()
    {
        $config = Config::getInstance();
        // mock response
        $return = sprintf('{"status":true,"message":"Retrieved application list url successfully.","data":"%s/apps/list"}', $config->getBaseUrl());
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->application()->getListPageUrl();
        // assertions
        $this->assertSame($response, json_decode($return, true)['data']);
    }

}
