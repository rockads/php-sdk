<?php

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\Suite;
use Faker\Factory;
use Faker\Generator;
use Rockads\Suite\Constants\ResultType;

class UserTest extends TestCase
{

    protected GuzzleHttp\Client $client;

    protected GuzzleHttp\Handler\MockHandler $mock;

    protected Token $token;

    protected Generator $faker;

    protected Suite $suite;

    public function setUp(): void
    {
        $this->faker = Factory::create();
        // token
        $this->token = new Token([
            "token_type" => "Bearer",
            "expires_in" => $this->faker->unixTime,
            "access_token" => $this->faker->uuid,
            "refresh_token" => $this->faker->uuid,
        ]);
        // config
        $config = Config::make([
            'base_url' => 'https://id.rockads.com',
            'client_id' => $this->faker->numberBetween(1, 10),
            'client_secret' => $this->faker->uuid,
            'oauth_redirect_uri' => sprintf('%s/oauth/redirect', $this->faker->domainName),
            'oauth_callback_uri' => sprintf('%s/oauth/callback', $this->faker->domainName),
        ]);
        // guzzle
        $mock = new MockHandler();
        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);
        $this->mock = $mock;
        // Suite
        $this->suite = new Suite($this->token, $config, $this->client);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function tesAll()
    {
        // mock response
        $returnList = '{"data":[{"id":"68411699396345856","name":"","username":null,"email":"wegw@hotmail.com","phone":null,"language":"tr","timezone":"UTC","last_login_date":null,"last_login_workspace_id":"1","avatar":null,"email_verified_at":null,"should_reset_password":true,"created_at":"2022-03-08 18:43:40","updated_at":"2022-03-08 18:43:40","services":[{"id":"68353954953560064","name":"3.platform","access_status":true,"service_role_id":"1","link":"https://zotlo-3api-backend.mobylonia.com/oauth/redirect","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/XQ7Ot9dbhRyNh2Bg5j0LWQqwypSx0iA8oLRX9Q63.png","created_at":"2022-03-08 14:54:12","updated_at":"2022-03-08 14:54:12"}],"customers":[{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":"pending","is_owner":0,"who_invited_id":1,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370524344320","name":"Supervisor","customer_id":"1","created_at":"2021-10-28 13:59:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370700505088","name":"Customer Create","is_allowed":true},{"id":"20867370784391168","name":"Customer Update","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371254153216","name":"User List","is_allowed":true},{"id":"20867371329650688","name":"User Create","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867372462112768","name":"Application Create","is_allowed":true},{"id":"20867372655050752","name":"Application Update","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373183533056","name":"Group Create","is_allowed":true},{"id":"20867373410025472","name":"Group View","is_allowed":true},{"id":"20867373640712192","name":"Group Update","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]}],"status":true,"message":"Users retrieved successfully."}';
        $returnPaginate = '{"data":[{"id":"68411699396345856","name":"","username":null,"email":"wegw@hotmail.com","phone":null,"language":"tr","timezone":"UTC","last_login_date":null,"last_login_workspace_id":"1","avatar":null,"email_verified_at":null,"should_reset_password":true,"created_at":"2022-03-08 18:43:40","updated_at":"2022-03-08 18:43:40","services":[{"id":"68353954953560064","name":"3.platform","access_status":true,"service_role_id":"1","link":"https://zotlo-3api-backend.mobylonia.com/oauth/redirect","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/XQ7Ot9dbhRyNh2Bg5j0LWQqwypSx0iA8oLRX9Q63.png","created_at":"2022-03-08 14:54:12","updated_at":"2022-03-08 14:54:12"}],"customers":[{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":"pending","is_owner":0,"who_invited_id":1,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370524344320","name":"Supervisor","customer_id":"1","created_at":"2021-10-28 13:59:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370700505088","name":"Customer Create","is_allowed":true},{"id":"20867370784391168","name":"Customer Update","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371254153216","name":"User List","is_allowed":true},{"id":"20867371329650688","name":"User Create","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867372462112768","name":"Application Create","is_allowed":true},{"id":"20867372655050752","name":"Application Update","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373183533056","name":"Group Create","is_allowed":true},{"id":"20867373410025472","name":"Group View","is_allowed":true},{"id":"20867373640712192","name":"Group Update","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]}],"links":{"first":"http://localhost/api/v1/users/paginate?page=1","last":"http://localhost/api/v1/users/paginate?page=91","prev":null,"next":"http://localhost/api/v1/users/paginate?page=2"},"meta":{"current_page":1,"from":1,"last_page":91,"links":[{"url":null,"label":"&laquo; Previous","active":false},{"url":"http://localhost/api/v1/users/paginate?page=1","label":"1","active":true},{"url":"http://localhost/api/v1/users/paginate?page=2","label":"2","active":false},{"url":"http://localhost/api/v1/users/paginate?page=3","label":"3","active":false},{"url":"http://localhost/api/v1/users/paginate?page=4","label":"4","active":false},{"url":"http://localhost/api/v1/users/paginate?page=5","label":"5","active":false},{"url":"http://localhost/api/v1/users/paginate?page=6","label":"6","active":false},{"url":"http://localhost/api/v1/users/paginate?page=7","label":"7","active":false},{"url":"http://localhost/api/v1/users/paginate?page=8","label":"8","active":false},{"url":"http://localhost/api/v1/users/paginate?page=9","label":"9","active":false},{"url":"http://localhost/api/v1/users/paginate?page=10","label":"10","active":false},{"url":null,"label":"...","active":false},{"url":"http://localhost/api/v1/users/paginate?page=90","label":"90","active":false},{"url":"http://localhost/api/v1/users/paginate?page=91","label":"91","active":false},{"url":"http://localhost/api/v1/users/paginate?page=2","label":"Next &raquo;","active":false}],"path":"http://localhost/api/v1/users/paginate","per_page":"1","to":1,"total":91},"status":true,"message":"Users retrieved successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnList));
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnPaginate));
        // get class instance & run method
        $responseList = $this->suite->user()->all(ResultType::LIST);
        $responsePaginate = $this->suite->user()->all(ResultType::PAGINATE);
        // assertions
        $this->assertSame($responseList, json_decode($returnList, true));
        $this->assertSame($responsePaginate, json_decode($returnPaginate, true));
    }

    public function testCreate()
    {
        // mock response
        $return = '{"data":{"id":"82062276055531520","name":"","username":null,"email":"asdasd@teknasyon.com","phone":null,"language":"tr","timezone":"UTC","last_login_date":null,"last_login_workspace_id":"2","avatar":null,"email_verified_at":null,"should_reset_password":true,"created_at":"2022-04-15 10:46:11","updated_at":"2022-04-15 10:46:11","services":[{"id":"1","name":"Marketing","access_status":true,"service_role_id":"101","link":"https://test-web.rockads.com/login","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"}],"customers":[{"id":"2","name":"Customer One","workspace":"customer_one","status":"pending","is_owner":0,"who_invited_id":37853166263336960,"invited_accept_at":null,"logged_in":false,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370545315840","name":"Member","customer_id":"2","created_at":"2021-10-28 14:00:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]},"status":true,"message":"User created successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // run method
        $response = $this->suite->user()->create([
            'customer_id' => $this->faker->randomNumber(),
            'email' => $this->faker->email(),
            'language' => $this->faker->locale,
            'services' => [
                'service_id' => $this->faker->randomNumber(),
                'role_id' => $this->faker->randomNumber(),
                'applications' => [
                    $this->faker->randomNumber(),
                    $this->faker->randomNumber(),
                ],
            ]
        ]);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testShow()
    {
        // mock response
        $return = '{"data":{"id":"82062276055531520","name":"","username":null,"email":"asdasd@teknasyon.com","phone":null,"language":"tr","timezone":"UTC","last_login_date":null,"last_login_workspace_id":"2","avatar":null,"email_verified_at":null,"should_reset_password":true,"created_at":"2022-04-15 10:46:11","updated_at":"2022-04-15 10:46:11","services":[{"id":"1","name":"Marketing","access_status":true,"service_role_id":"101","link":"https://test-web.rockads.com/login","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"}],"customers":[{"id":"2","name":"Customer One","workspace":"customer_one","status":"pending","is_owner":0,"who_invited_id":37853166263336960,"invited_accept_at":null,"logged_in":false,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370545315840","name":"Member","customer_id":"2","created_at":"2021-10-28 14:00:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]},"status":true,"message":"User created successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // run method
        $response = $this->suite->user()->show(82062276055531520);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testUpdate()
    {
        // mock response
        $return = '{"data":{"id":"82062276055531520","name":"","username":null,"email":"asdasd@teknasyon.com","phone":null,"language":"tr","timezone":"UTC","last_login_date":null,"last_login_workspace_id":"2","avatar":null,"email_verified_at":null,"should_reset_password":true,"created_at":"2022-04-15 10:46:11","updated_at":"2022-04-15 10:46:11","services":[{"id":"1","name":"Marketing","access_status":true,"service_role_id":"101","link":"https://test-web.rockads.com/login","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"}],"customers":[{"id":"2","name":"Customer One","workspace":"customer_one","status":"pending","is_owner":0,"who_invited_id":37853166263336960,"invited_accept_at":null,"logged_in":false,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370545315840","name":"Member","customer_id":"2","created_at":"2021-10-28 14:00:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]},"status":true,"message":"User created successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // run method
        $response = $this->suite->user()->update(82062276055531520, [
            'language' => $this->faker->locale,
            'services' => [
                'service_id' => $this->faker->randomNumber(),
                'role_id' => $this->faker->randomNumber(),
            ]
        ]);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testDelete()
    {
        // mock response
        $return = '{"status":true,"message":"User deleted successfully.","data":[]}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->user()->destroy(8206227605553152);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testGetCreatePageUrl()
    {
        $config = Config::getInstance();
        $continue = $this->faker->domainName.'/user/list';
        // mock response
        $return = sprintf('{"status":true,"message":"Retrieved user create url successfully.","data":"%s/user/add"}', $config->getBaseUrl());
        $returnWithContinue = sprintf('{"status":true,"message":"Retrieved user list url successfully.","data":"%s/user/add?continue=%s"}', $config->getBaseUrl(), $continue);
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnWithContinue));
        // get class instance & run method
        $response = $this->suite->user()->getCreatePageUrl();
        $responseWithContinue = $this->suite->user()->getCreatePageUrl($continue);
        // assertions
        $this->assertSame($response, json_decode($return, true)['data']);
        $this->assertSame($responseWithContinue, json_decode($returnWithContinue, true)['data']);
    }

    public function testGetListPageUrl()
    {
        $config = Config::getInstance();
        // mock response
        $return = sprintf('{"status":true,"message":"Retrieved user list url successfully.","data":"%s/user/list"}', $config->getBaseUrl());
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->user()->getListPageUrl();
        // assertions
        $this->assertSame($response, json_decode($return, true)['data']);
    }
}
