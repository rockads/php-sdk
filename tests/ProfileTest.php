<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Faker\Factory;
use Faker\Generator;
use Rockads\Suite\Suite;

class ProfileTest extends TestCase
{

    protected GuzzleHttp\Client $client;

    protected GuzzleHttp\Handler\MockHandler $mock;

    protected Token $token;

    protected Generator $faker;

    protected Suite $suite;

    public function setUp(): void
    {
        $this->faker = Factory::create();
        // token
        $this->token = new Token([
            "token_type" => "Bearer",
            "expires_in" => $this->faker->unixTime,
            "access_token" => $this->faker->uuid,
            "refresh_token" => $this->faker->uuid,
        ]);
        // config
        $config = Config::make([
            'base_url' => 'https://id.rockads.com',
            'client_id' => $this->faker->numberBetween(1, 10),
            'client_secret' => $this->faker->uuid,
            'oauth_redirect_uri' => sprintf('%s/oauth/redirect', $this->faker->domainName),
            'oauth_callback_uri' => sprintf('%s/oauth/callback', $this->faker->domainName),
        ]);
        // guzzle
        $mock = new MockHandler();
        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);
        $this->mock = $mock;
        // Suite
        $this->suite = new Suite($this->token, $config, $this->client);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function testMe()
    {
        // mock response
        $return = '{"data":{"id":"37853166263336960","name":"amir","username":null,"email":"amirshadanfar@teknasyon.com","phone":"12345678912","language":"en","timezone":"Europe/Istanbul","last_login_date":"2022-04-04 12:37:14","last_login_workspace_id":"1","avatar":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/users_avatar/AK8izJ2by169Svcu1nl7Qc6M9PjeHJ2rRBayjmmK.jpg","email_verified_at":"2022-01-11 17:13:43","should_reset_password":false,"created_at":"2021-12-14 10:54:58","updated_at":"2022-04-04 12:37:14","services":[{"id":"1","name":"Marketing","access_status":true,"service_role_id":"222","link":"https://test-web.rockads.com/login","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"}],"customers":[{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":"active","is_owner":1,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370503372800","name":"Owner","customer_id":"1","created_at":"2021-10-28 13:58:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370700505088","name":"Customer Create","is_allowed":true},{"id":"20867370784391168","name":"Customer Update","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371254153216","name":"User List","is_allowed":true},{"id":"20867371329650688","name":"User Create","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371770052608","name":"User Delete","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867372462112768","name":"Application Create","is_allowed":true},{"id":"20867372655050752","name":"Application Update","is_allowed":true},{"id":"20867372864765952","name":"Application Delete","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373183533056","name":"Group Create","is_allowed":true},{"id":"20867373410025472","name":"Group View","is_allowed":true},{"id":"20867373640712192","name":"Group Update","is_allowed":true},{"id":"20867373888176128","name":"Group Delete","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[{"id":"31428899929849856","name":"iFind","icon":"https://placebeard.it/640x360","customer_id":1,"created_at":"2021-11-26 17:27:13","updated_at":"2021-11-26 17:27:13","service_id":1},{"id":"32806590939660288","name":"app 7","icon":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/applications_icon/kesuLv2PG6JxsbDJ4giATFZxdMfGtldWrnnIPM3N.png","customer_id":1,"created_at":"2021-11-30 12:41:40","updated_at":"2021-12-01 11:36:06","service_id":1},{"id":"33919803051737088","name":"saasdasd","icon":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/applications_icon/RmogrhE2pjN5kevz8OjYnF5dRGulGdj5CawIK3dr.png","customer_id":1,"created_at":"2021-12-03 14:25:11","updated_at":"2021-12-03 14:25:11","service_id":8},{"id":"73784274087575552","name":"get contact","icon":null,"customer_id":1,"created_at":"2022-03-23 14:32:21","updated_at":"2022-03-23 14:32:21","service_id":1},{"id":"73784328231845888","name":"get contact","icon":null,"customer_id":1,"created_at":"2022-03-23 14:32:34","updated_at":"2022-03-23 14:32:34","service_id":1},{"id":"73785312941178880","name":"get contact 2 updated","icon":null,"customer_id":1,"created_at":"2022-03-23 14:36:29","updated_at":"2022-03-23 14:45:22","service_id":1}]},{"id":"2","name":"Customer One","workspace":"customer_one","status":"active","is_owner":0,"who_invited_id":null,"invited_accept_at":null,"logged_in":false,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370545315840","name":"Member","customer_id":"2","created_at":"2021-10-28 14:00:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]},"status":true,"message":"User retrieve successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->profile()->me();
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testLogout()
    {
        // mock response
        $return = '{"status": true,"message": "User\'s token revoked successfully.","data": []}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->profile()->logout();
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testChangePassword()
    {
        // mock response
        $return = '{"status": true,"message": "Password changed successfully.","data": []}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->profile()->changePassword('123', 'asd123!@#');
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testUpdate()
    {
        // mock response
        $return = '{"data":{"id":"37853166263336960","name":"1111","username":null,"email":"amirshadanfar@teknasyon.com","phone":"12345678912","language":"en","timezone":"Europe/Istanbul","last_login_date":"2022-04-08 13:50:33","last_login_workspace_id":"1","avatar":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/users_avatar/AK8izJ2by169Svcu1nl7Qc6M9PjeHJ2rRBayjmmK.jpg","email_verified_at":"2022-01-11 17:13:43","should_reset_password":false,"created_at":"2021-12-14 10:54:58","updated_at":"2022-04-08 13:51:14","services":[{"id":"1","name":"Marketing","access_status":true,"service_role_id":"222","link":"https://test-web.rockads.com/login","logo":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/services_logo/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"}],"customers":[{"id":"1","name":"Teknasyon","workspace":"teknasyon","status":"active","is_owner":1,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370503372800","name":"Owner","customer_id":"1","created_at":"2021-10-28 13:58:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370700505088","name":"Customer Create","is_allowed":true},{"id":"20867370784391168","name":"Customer Update","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371254153216","name":"User List","is_allowed":true},{"id":"20867371329650688","name":"User Create","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371770052608","name":"User Delete","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867372462112768","name":"Application Create","is_allowed":true},{"id":"20867372655050752","name":"Application Update","is_allowed":true},{"id":"20867372864765952","name":"Application Delete","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373183533056","name":"Group Create","is_allowed":true},{"id":"20867373410025472","name":"Group View","is_allowed":true},{"id":"20867373640712192","name":"Group Update","is_allowed":true},{"id":"20867373888176128","name":"Group Delete","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[{"id":"31428899929849856","name":"iFind","icon":"https://placebeard.it/640x360","customer_id":1,"created_at":"2021-11-26 17:27:13","updated_at":"2021-11-26 17:27:13","service_id":1},{"id":"32806590939660288","name":"app 7","icon":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/applications_icon/kesuLv2PG6JxsbDJ4giATFZxdMfGtldWrnnIPM3N.png","customer_id":1,"created_at":"2021-11-30 12:41:40","updated_at":"2021-12-01 11:36:06","service_id":1},{"id":"33919803051737088","name":"saasdasd","icon":"https://suite-stage-application-assets.s3.eu-west-1.amazonaws.com/applications_icon/RmogrhE2pjN5kevz8OjYnF5dRGulGdj5CawIK3dr.png","customer_id":1,"created_at":"2021-12-03 14:25:11","updated_at":"2021-12-03 14:25:11","service_id":8},{"id":"73784274087575552","name":"get contact","icon":null,"customer_id":1,"created_at":"2022-03-23 14:32:21","updated_at":"2022-03-23 14:32:21","service_id":1},{"id":"73784328231845888","name":"get contact","icon":null,"customer_id":1,"created_at":"2022-03-23 14:32:34","updated_at":"2022-03-23 14:32:34","service_id":1},{"id":"73785312941178880","name":"get contact 2 updated","icon":null,"customer_id":1,"created_at":"2022-03-23 14:36:29","updated_at":"2022-03-23 14:45:22","service_id":1}]},{"id":"2","name":"Customer One","workspace":"customer_one","status":"active","is_owner":0,"who_invited_id":null,"invited_accept_at":null,"logged_in":false,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","role":{"id":"20867370545315840","name":"Member","customer_id":"2","created_at":"2021-10-28 14:00:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]},"status":true,"message":"User updated successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->profile()->update([
            'name' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'timezone' => $this->faker->timezone,
            'language' => $this->faker->locale,
            'avatar' => $this->faker->image
        ]);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testGetProfilePageUrl()
    {
        $config = Config::getInstance();
        $continue = $this->faker->domainName.'/dashboard';
        // mock response
        $return = sprintf('{"status":true,"message":"Retrieved user profile url successfully.","data":"%s/settings/profile"}', $config->getBaseUrl());
        $returnWithContinue = sprintf('{"status":true,"message":"Retrieved user profile url successfully.","data":"%s/settings/profile?continue=%s"}', $config->getBaseUrl(), $continue);
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnWithContinue));
        // get class instance & run method
        $response = $this->suite->profile()->getProfilePageUrl();
        $responseWithContinue = $this->suite->profile()->getProfilePageUrl($continue);
        // assertions
        $this->assertSame($response, json_decode($return, true)['data']);
        $this->assertSame($responseWithContinue, json_decode($returnWithContinue, true)['data']);
    }
}
