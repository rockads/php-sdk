<?php

use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\OAuth;

class OAuthTest extends TestCase
{
    protected GuzzleHttp\Client $client;

    protected GuzzleHttp\Handler\MockHandler $mock;

    protected Generator $faker;

    protected OAuth $oAuth;

    public function setUp(): void
    {
        $this->faker = Factory::create();
        // config
        $config = Config::make([
            'base_url' => 'https://id.rockads.com',
            'client_id' => $this->faker->numberBetween(1, 10),
            'client_secret' => $this->faker->uuid,
            'oauth_redirect_uri' => sprintf('%s/oauth/redirect', $this->faker->domainName),
            'oauth_callback_uri' => sprintf('%s/oauth/callback', $this->faker->domainName),
        ]);
        // guzzle
        $mock = new MockHandler();
        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);
        $this->mock = $mock;
        // OAuth
        $this->oAuth = new OAuth($config, $this->client);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function testGetLoginUrl()
    {
        // mock response
        $config = Config::getInstance();
        $continue = $config->getRedirectUri();
        $return = sprintf('{"status": true,"message": "Retrieved login url successfully.","data": "%s/auth/login?continue=%s"}', $config->getBaseUrl(), $continue);
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->oAuth->getLoginUrl($continue);
        // assertions
        $this->assertEquals($response, json_decode($return, true)['data']);
    }

    public function testGetRegisterUrl()
    {
        // mock response
        $config = Config::getInstance();
        $continue = $config->getRedirectUri();
        $return = sprintf('{"status": true,"message": "Retrieved register url successfully.","data": "%s/auth/register?continue=%s"}', $config->getBaseUrl(), $continue);
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->oAuth->getRegisterUrl($continue);
        // assertions
        $this->assertEquals($response, json_decode($return, true)['data']);
    }

    public function testGetRedirectUrl()
    {
        // mock response
        $state = generateRandomString(40);
        $config = Config::getInstance();
        $return = sprintf('{"status": true,"message": "Retrieved redirect url successfully.","data": "%s/oauth/authorize?response_type=code&state=%s&client_id=%s&oauth_redirect_uri=%s"}',
            $config->getBaseUrl(),
            $state,
            $config->getClientId(),
            $config->getCallbackUri(),
        );
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->oAuth->getRedirectUrl($state);
        // assertions
        $this->assertEquals($response, json_decode($return, true)['data']);
    }

    public function testGetTokenByExchangeCode()
    {
        // mock response
        $return = '{"token_type":"Bearer","expires_in":1649503271,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxMyIsImp0aSI6IjMyOWRmMDdhOTc1ZDhjZDJjNzBmOGU5MGM4NGM3YTIzMmI2MzNiNGY3ZDRmNDZmM2YzMTA1NWY1YzU5ZTk2YjJkZDdkNzAzOGMxN2JhOGRkIiwiaWF0IjoxNjQ5MzIwMjk2Ljk1MTQ1OCwibmJmIjoxNjQ5MzIwMjk2Ljk1MTQ2MiwiZXhwIjoxNjQ5NDkzMDk2LjkzNTU4OCwic3ViIjoiMzc4NTMxNjYyNjMzMzY5NjAiLCJzY29wZXMiOltdfQ.2Sb2bPUYbWy4UL2fppfkQ1Ha0iUf1ApZboPAhRGVTCENlW5J7dF4AhF6TTfFFqlwc8B1aWkxnCASZgNMx08q2fguXlB8BnZLTEubKVWUF3O8kqs-koxCDyPzDL7ZxTdJ8zmVVN_HodbZ7_YI_Q54F_pIBD7SWnMQyZgX8KDZsNF0kQs14P0nZhKpKBtV6XXnkHkENnObfVeqcTWHuMytUVSajMKBgWA7300V-Fs753CfmtM94Qj9Wv9cAiybd9g5KZS3LNA_8yLVQsqO9Z72xpMUCNpMYwRSjAey49IgVhXiLv4vC4sfp5iTcJdVaR4G2dDoIP3EmyRBCVh0k9WqvWuRPpKIe5nSxRszChJQ7CLlKFNM7hdc7YVc-EBa2DRfO9ZNjb2wJSeaCJm2SBrEf1UeKKcVnKCR461DE011cA0Rf5gw9Go1b5ucyOg2mpDYzoFyuv4qUHbtaYmHIdkaX9-fZNg3BhnCsOjrq-maEbvmZUMOSgIQxyKbzpByu0ilrYqBPGNq1E-6Od46HYGPKRfI-sFSLBdeCWnI0Etz6kDU_W72SMlCrEsCGzyJH_W21MxAZLhbzIFtBglfPPPMqkc5FS5DvJAVqwTD2Du6B_Jh-Vry2B5z2WTNfqxE-1-a7loyozDkIuUpEGJ79DPe2mh27q4Na0p9-0Tn8_3rqts","refresh_token":"def50200ffe1934172905b98eec14c90d56d4b260ec839fcb1f1b7cd4cd9a93e1ec49d715881fa445d822fd31fb9f82c4a73b68b87f6efca07e1e5813fc547a347148bf0304331e604de1118c75ba39af3e37b1f22df3b94e6a10e913a3131fa287b3d922eff5f6f0ab840c06019d2e2654453d7a33c6cfff01d5b38264d50227656a051d0bbc39f215586f71f7a8cf5815f7ab75271d450f7134a687ed6a6a98b8e31b0d5625ed3bc28b47684005c435ae81459eccfbfa9bfb7323430e69ecb3eaa98adaf760dfbddb03097f746eb6e2d35d8d84f7c03bbbabce906a1f26f7f1f6074e868bbea2d454d8fdb28d38f91b1299c02f38c9c47131406468ef552cd89c844fb497c8d3550caa4ecc6dea0ef1e0d562d441c12f4d225053975a244c2d870df871d1f7421c581d88b20544b74ce36f027edc96b90d2ccc63369a7bbe512813f5ed491a523e8e3db276e2c17e5c682d2592a86148fe5ed2ad82bca625a3cb475c55453ed1ba7da4060d6d3ee61a25798aa"}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $exchangeCode = 'ef502002ca4c33565f42b1c153954c7cd4adc5127d800ac570a19caf573b94ad5f716a31eca3ec8d6e76b73eb6c4266b72fd59ab5971a4f5b2084573c2973d3379dff8cefbe946c652c1e556778102861fb133487dbb68500c53fc984b9e1b6bd7230c8eed85df9fdadffe60ae80f61a9a79bc7a8b8eb11862f8057169dee3bf635509e26bd141c692df639abb249ca31a2987e3ed530cb4b5725f8c3d4835ad5a2b404b2d95e8075d2736ad962c8e22352fa14b86502e4ab6de451d5d2f9142861d7b9b6572707e630b248c50a1a63f8650623ce829228be71d5d5a211b2726812f65d02cc25771c6ad9551d151025e26f7b6805a1d02bfd593e932b893eebf34908dd66b181171fd38d65d46cb28d4ef97b5c1844ead2f0beaf573fe7fec0903ca46beff6af8351d227403d926be090bddb544b6ee40702516af2aee960b6bc199ea6f2b61e75edcf063c9262929f5e7b ◀def502002ca4c33565f42b1c153954c7cd4adc5127d800ac570a19caf573b94ad5f716a31eca3ec8d6e76b73eb6c4266b72fd59ab5971a4f5b2084573c2973d3379dff8cefbe946c652c1e5567781028';
        $response = $this->oAuth->getTokenByExchangeCode($exchangeCode);
        // assertions
        $this->assertInstanceOf(Token::class, $response);
        $this->assertEquals($response->getTokenType(), json_decode($return, true)['token_type']);
        $this->assertEquals($response->getAccessToken(), json_decode($return, true)['access_token']);
        $this->assertEquals($response->getRefreshToken(), json_decode($return, true)['refresh_token']);
    }

    public function testRefreshToken()
    {
        // mock response
        $return = '{"token_type":"Bearer","expires_in":1649503271,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxMyIsImp0aSI6IjMyOWRmMDdhOTc1ZDhjZDJjNzBmOGU5MGM4NGM3YTIzMmI2MzNiNGY3ZDRmNDZmM2YzMTA1NWY1YzU5ZTk2YjJkZDdkNzAzOGMxN2JhOGRkIiwiaWF0IjoxNjQ5MzIwMjk2Ljk1MTQ1OCwibmJmIjoxNjQ5MzIwMjk2Ljk1MTQ2MiwiZXhwIjoxNjQ5NDkzMDk2LjkzNTU4OCwic3ViIjoiMzc4NTMxNjYyNjMzMzY5NjAiLCJzY29wZXMiOltdfQ.2Sb2bPUYbWy4UL2fppfkQ1Ha0iUf1ApZboPAhRGVTCENlW5J7dF4AhF6TTfFFqlwc8B1aWkxnCASZgNMx08q2fguXlB8BnZLTEubKVWUF3O8kqs-koxCDyPzDL7ZxTdJ8zmVVN_HodbZ7_YI_Q54F_pIBD7SWnMQyZgX8KDZsNF0kQs14P0nZhKpKBtV6XXnkHkENnObfVeqcTWHuMytUVSajMKBgWA7300V-Fs753CfmtM94Qj9Wv9cAiybd9g5KZS3LNA_8yLVQsqO9Z72xpMUCNpMYwRSjAey49IgVhXiLv4vC4sfp5iTcJdVaR4G2dDoIP3EmyRBCVh0k9WqvWuRPpKIe5nSxRszChJQ7CLlKFNM7hdc7YVc-EBa2DRfO9ZNjb2wJSeaCJm2SBrEf1UeKKcVnKCR461DE011cA0Rf5gw9Go1b5ucyOg2mpDYzoFyuv4qUHbtaYmHIdkaX9-fZNg3BhnCsOjrq-maEbvmZUMOSgIQxyKbzpByu0ilrYqBPGNq1E-6Od46HYGPKRfI-sFSLBdeCWnI0Etz6kDU_W72SMlCrEsCGzyJH_W21MxAZLhbzIFtBglfPPPMqkc5FS5DvJAVqwTD2Du6B_Jh-Vry2B5z2WTNfqxE-1-a7loyozDkIuUpEGJ79DPe2mh27q4Na0p9-0Tn8_3rqts","refresh_token":"def50200ffe1934172905b98eec14c90d56d4b260ec839fcb1f1b7cd4cd9a93e1ec49d715881fa445d822fd31fb9f82c4a73b68b87f6efca07e1e5813fc547a347148bf0304331e604de1118c75ba39af3e37b1f22df3b94e6a10e913a3131fa287b3d922eff5f6f0ab840c06019d2e2654453d7a33c6cfff01d5b38264d50227656a051d0bbc39f215586f71f7a8cf5815f7ab75271d450f7134a687ed6a6a98b8e31b0d5625ed3bc28b47684005c435ae81459eccfbfa9bfb7323430e69ecb3eaa98adaf760dfbddb03097f746eb6e2d35d8d84f7c03bbbabce906a1f26f7f1f6074e868bbea2d454d8fdb28d38f91b1299c02f38c9c47131406468ef552cd89c844fb497c8d3550caa4ecc6dea0ef1e0d562d441c12f4d225053975a244c2d870df871d1f7421c581d88b20544b74ce36f027edc96b90d2ccc63369a7bbe512813f5ed491a523e8e3db276e2c17e5c682d2592a86148fe5ed2ad82bca625a3cb475c55453ed1ba7da4060d6d3ee61a25798aa"}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $oldRefreshToken = 'def502002ae49aceda1df06e93bde04574c1d76fa4b7738ea6c40666ae1f64b7e2e0d95795ec4185137183fda75c6c492335f058e244544d0f39de5de42a683da218304a99787cac0501e23f09764361e0ea9dd53d02ff839e54201823298e0b095a3649e40f1d844e8d911c456fc1861f14cc9968e0a2784fc3e9fc32448e80585f2d6e26c74260c56879ee5c5580b0af6509a7c96ee1505621f60bc019fe0f6936b9aba8c09200d5d5b2c0b4752a59a2586d28c27630ed1fa00e86def15b74ab11d7264971f4a0062693d7e674f05cc12239ce4f21648f9ebc231b94fc28b1ae579295cb9978f2978e0d2e6def691fac9416246798335b119918205acf62b0173ade1e629ac887781676b502b284244ae55289e981bf5175ea0f5e99175d349853f296dc43f1e387a840ec714d896731ff9ba0b7dc9c447edbf718541787d52fa6f5a0f7fa740ef1ed96ddd0c55ae2c46c28bc1204539b038d5a6d50a620a41857c0c2baae3cb10a8308daf8a3baaeeac9d49a';
        $response = $this->oAuth->refreshToken($oldRefreshToken);
        // assertions
        $this->assertInstanceOf(Token::class, $response);
        $this->assertEquals($response->getTokenType(), json_decode($return, true)['token_type']);
        $this->assertEquals($response->getAccessToken(), json_decode($return, true)['access_token']);
        $this->assertEquals($response->getRefreshToken(), json_decode($return, true)['refresh_token']);
    }
}
