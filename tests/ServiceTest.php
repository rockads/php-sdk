<?php

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Rockads\Suite\Constants\ResultType;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\Suite;
use Faker\Factory;
use Faker\Generator;

class ServiceTest extends TestCase
{

    protected GuzzleHttp\Client $client;

    protected GuzzleHttp\Handler\MockHandler $mock;

    protected Token $token;

    protected Generator $faker;

    protected Suite $suite;

    public function setUp(): void
    {
        $this->faker = Factory::create();
        // token
        $this->token = new Token([
            "token_type" => "Bearer",
            "expires_in" => $this->faker->unixTime,
            "access_token" => $this->faker->uuid,
            "refresh_token" => $this->faker->uuid,
        ]);
        // config
        $config = Config::make([
            'base_url' => 'https://id.rockads.com',
            'client_id' => $this->faker->numberBetween(1, 10),
            'client_secret' => $this->faker->uuid,
            'oauth_redirect_uri' => sprintf('%s/oauth/redirect', $this->faker->domainName),
            'oauth_callback_uri' => sprintf('%s/oauth/callback', $this->faker->domainName),
        ]);
        // guzzle
        $mock = new MockHandler();
        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);
        $this->mock = $mock;
        // Suite
        $this->suite = new Suite($this->token, $config, $this->client);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function tesAll()
    {
        // mock response
        $returnList = '{"data":[{"id":"68353954953560064","name":"3.platform","access_status":false,"service_role_id":null,"link":"https:\/\/zotlo-3api-backend.mobylonia.com\/oauth\/redirect","logo":"https:\/\/suite-stage-application-assets.s3.eu-west-1.amazonaws.com\/services_logo\/XQ7Ot9dbhRyNh2Bg5j0LWQqwypSx0iA8oLRX9Q63.png","created_at":"2022-03-08 14:54:12","updated_at":"2022-03-08 14:54:12"},{"id":"1","name":"Marketing","access_status":true,"service_role_id":"222","link":"https:\/\/test-web.rockads.com\/login","logo":"https:\/\/suite-stage-application-assets.s3.eu-west-1.amazonaws.com\/services_logo\/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"},{"id":"3","name":"DataSuite","access_status":false,"service_role_id":null,"link":"https:\/\/stage-data-suite.rockads.com\/api\/v1\/suite\/oauth\/redirect","logo":"https:\/\/suite-stage-application-assets.s3.eu-west-1.amazonaws.com\/services_logo\/mhj0ExYu9oL0lVDTIhg6kFYqdjhvRB39yBIuobCs.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 11:56:55"}],"status":true,"message":"Services retrieved successfully."}';
        $returnPaginate = '{"data":[{"id":"68353954953560064","name":"3.platform","access_status":false,"service_role_id":null,"link":"https:\/\/zotlo-3api-backend.mobylonia.com\/oauth\/redirect","logo":"https:\/\/suite-stage-application-assets.s3.eu-west-1.amazonaws.com\/services_logo\/XQ7Ot9dbhRyNh2Bg5j0LWQqwypSx0iA8oLRX9Q63.png","created_at":"2022-03-08 14:54:12","updated_at":"2022-03-08 14:54:12"},{"id":"1","name":"Marketing","access_status":true,"service_role_id":"222","link":"https:\/\/test-web.rockads.com\/login","logo":"https:\/\/suite-stage-application-assets.s3.eu-west-1.amazonaws.com\/services_logo\/LPvqspAJRxbOEg15J9FrgYpJSF4ph8wXSxyVx1a4.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 12:09:29"},{"id":"3","name":"DataSuite","access_status":false,"service_role_id":null,"link":"https:\/\/stage-data-suite.rockads.com\/api\/v1\/suite\/oauth\/redirect","logo":"https:\/\/suite-stage-application-assets.s3.eu-west-1.amazonaws.com\/services_logo\/mhj0ExYu9oL0lVDTIhg6kFYqdjhvRB39yBIuobCs.png","created_at":"2021-06-16 15:00:37","updated_at":"2022-03-08 11:56:55"}],"links":{"first":"http:\/\/localhost\/api\/v1\/services\/paginate?page=1","last":"http:\/\/localhost\/api\/v1\/services\/paginate?page=1","prev":null,"next":null},"meta":{"current_page":1,"from":1,"last_page":1,"links":[{"url":null,"label":"&laquo; Previous","active":false},{"url":"http:\/\/localhost\/api\/v1\/services\/paginate?page=1","label":"1","active":true},{"url":null,"label":"Next &raquo;","active":false}],"path":"http:\/\/localhost\/api\/v1\/services\/paginate","per_page":"5","to":3,"total":3},"status":true,"message":"Services retrieved successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnList));
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $returnPaginate));
        // get class instance & run method
        $responseList = $this->suite->service()->all(ResultType::LIST);
        $responsePaginate = $this->suite->service()->all(ResultType::PAGINATE);
        // assertions
        $this->assertSame($responseList, json_decode($returnList, true));
        $this->assertSame($responsePaginate, json_decode($returnPaginate, true));
    }

    public function testShow()
    {
        // mock response
        $return = '{"data":{"id":"68353954953560064","name":"Postman","access_status":true,"service_role_id":null,"link":null,"logo":null,"created_at":"2021-06-16 15:00:37","updated_at":"2021-06-16 15:00:37","client":{"id":"9","secret":"bDuwhdU8rnyVgt8SnJbrEpteafhbWCK8Djz6TaWb","oauth_callback_url":"https:\/\/oauth.pstmn.io\/v1\/callback","personal_access_client":0,"password_client":0,"revoked":0},"applications":[{"id":"31429030322372608","name":"Calley AI","icon":"https:\/\/placebeard.it\/640x360","customer_id":30623388636545024,"created_at":"2021-11-26 17:27:44","updated_at":"2021-11-26 17:27:44","service_id":6}],"users":[],"customers":[]},"status":true,"message":"Service retrieved successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // run method
        $response = $this->suite->application()->show(68353954953560064);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

}
