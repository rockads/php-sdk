<?php

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\Suite;
use Faker\Factory;
use Faker\Generator;

class CustomerTest extends TestCase
{

    protected GuzzleHttp\Client $client;

    protected GuzzleHttp\Handler\MockHandler $mock;

    protected Token $token;

    protected Generator $faker;

    protected Suite $suite;

    public function setUp(): void
    {
        $this->faker = Factory::create();
        // token
        $this->token = new Token([
            "token_type" => "Bearer",
            "expires_in" => $this->faker->unixTime,
            "access_token" => $this->faker->uuid,
            "refresh_token" => $this->faker->uuid,
        ]);
        // config
        $config = Config::make([
            'base_url' => 'https://id.rockads.com',
            'client_id' => $this->faker->numberBetween(1, 10),
            'client_secret' => $this->faker->uuid,
            'oauth_redirect_uri' => sprintf('%s/oauth/redirect', $this->faker->domainName),
            'oauth_callback_uri' => sprintf('%s/oauth/callback', $this->faker->domainName),
        ]);
        // guzzle
        $mock = new MockHandler();
        $handlerStack = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handlerStack]);
        $this->mock = $mock;
        // Suite
        $this->suite = new Suite($this->token, $config, $this->client);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function testRegister()
    {
        // mock response
        $return = '{"status":true,"message":"Customer & it\'s user registered successfully.","data":{"id":"79642335519965184","name":"emir","username":null,"email":"emir@teknasyon.com","phone":null,"language":null,"timezone":"UTC","last_login_date":"2022-04-08 18:30:12","last_login_workspace_id":"79642334899208192","avatar":null,"email_verified_at":null,"should_reset_password":true,"created_at":"2022-04-08 18:30:12","updated_at":"2022-04-08 18:30:12","services":[],"customers":[{"id":"79642334899208192","name":"amir customer","workspace":"amir-customer","status":"active","is_owner":1,"who_invited_id":null,"invited_accept_at":null,"logged_in":false,"created_at":"2022-04-08 18:30:12","updated_at":"2022-04-08 18:30:12","role":{"id":"20867370503372800","name":"Owner","customer_id":"79642334899208192","created_at":"2021-10-28 13:58:00","updated_at":"2021-10-28 13:59:28","permissions":[{"id":"20867370662756352","name":"Customer View","is_allowed":true},{"id":"20867370700505088","name":"Customer Create","is_allowed":true},{"id":"20867370784391168","name":"Customer Update","is_allowed":true},{"id":"20867370885054464","name":"Service List","is_allowed":true},{"id":"20867370935386112","name":"Service View","is_allowed":true},{"id":"20867371254153216","name":"User List","is_allowed":true},{"id":"20867371329650688","name":"User Create","is_allowed":true},{"id":"20867371468062720","name":"User View","is_allowed":true},{"id":"20867371614863360","name":"User Update","is_allowed":true},{"id":"20867371770052608","name":"User Delete","is_allowed":true},{"id":"20867371845550080","name":"Acl List","is_allowed":true},{"id":"20867372462112768","name":"Application Create","is_allowed":true},{"id":"20867372655050752","name":"Application Update","is_allowed":true},{"id":"20867372864765952","name":"Application Delete","is_allowed":true},{"id":"20867373074481152","name":"Group List","is_allowed":true},{"id":"20867373183533056","name":"Group Create","is_allowed":true},{"id":"20867373410025472","name":"Group View","is_allowed":true},{"id":"20867373640712192","name":"Group Update","is_allowed":true},{"id":"20867373888176128","name":"Group Delete","is_allowed":true},{"id":"20867373888176129","name":"Application List","is_allowed":true},{"id":"20867373888176130","name":"Application View","is_allowed":true}]},"applications":[]}]}}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->customer()->register([
            'name' => $this->faker->name,
            'workspace' => $this->faker->unique()->name,
        ], [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'language' => $this->faker->locale,
        ]);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }

    public function testUpdate()
    {
        // mock response
        $return = '{"data":{"id":"79642334899208192","name":"amir customer","workspace":"amir-customer","status":null,"is_owner":false,"who_invited_id":null,"invited_accept_at":null,"logged_in":true,"created_at":"2022-04-08 18:30:12","updated_at":"2022-04-08 18:30:12"},"status":true,"message":"Customers retrieved successfully."}';
        $this->mock->append(new Response(200, ['content-type' => 'application/json'], $return));
        // get class instance & run method
        $response = $this->suite->customer()->update(79642334899208192, $this->faker->name, $this->faker->unique()->name);
        // assertions
        $this->assertSame($response, json_decode($return, true));
    }
}
