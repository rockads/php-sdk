# Working with applications

In Creating or Updating applications maybe get duplicate validation error, so it is important to create application with unique
name and platform bundle id inside the workspace.

## Make instance of Suite class

<b>Notice:</b> If you are using <b>Laravel</b>, SuiteServiceProvider bind <b>oAuth</b> and <b>Suite</b> automatically as
a Singleton and you will not get any errors about dependency injection.

Otherwise, you should bind this two class in your framework <b>service container</b> manually or just 'new' these class
inside your methods.

* Laravel:

```php
 use Rockads\Suite\Suite;
 use Rockads\Suite\Models\Config;
 
 class ApplicationController {
 
      protected $suite;
 
      public function __construct(Suite $suite)
      {
          // get token
          $userId = auth()->user()->id;
          $token = $this->getToken($userId);
          $suite->setToken($token);
    
          $this->suite = $suite;
      }
```

* Other frameworks or pure PHP without injecting Suite SDK dependencies:

```php
 use Rockads\Suite\Suite;
 use Rockads\Suite\Models\Config;
 use GuzzleHttp\Client;
 
 class ApplicationController {
    
      protected Suite $suite;

      public function __construct()
      {      
          $client = new Client();
          $config = Config::make(config('suite'));   
          // get token
          $userId = auth()->user()->id;
          $token = $this->getToken($userId); 

          $this->suite = new Suite($token, $config, $client);
      }
```

Notice: User should have been authenticated in all module controllers like Application, Profile and User. (protected
routes)

So we have user id, and we can pass it to getToken method to get token which is got from oAuth procedure:

```php
  protected function getToken($userId): Token
  {
      if (Cache::has('token_' . $userId))
          return Cache::get('token_' . $userId);
      else
          throw new \Exception('Token is not exist');
  }
```

## Get applications

```php
 use Rockads\Suite\Constants\ResultType;
  
 public function all()
 {
    try {      
        $resultType = $this->faker->randomElement(ResultType::toArray());
        $response = $this->suite->application()->all($resultType);
        return json_encode($response);
    } catch (\Throwable $exception) {
        if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
            http_response_code($exception->getCode());
            return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
        }
        return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
    }
 }
```

## create application

For creating an application name is enough to get id. platforms can be assigned later with update method:

```php
 use Rockads\Suite\Constants\PlatformType;
 use Rockads\Suite\Constants\ProcessType;
 
 public function create()
 {
    try {        
        $appName = $this->faker->name;
        $appPlatforms = [
          ['platform' => PlatformType::ANDROID, 'bundle_id' => $this->faker->randomDigit()],
          ['platform' => PlatformType::IOS, 'bundle_id' => $this->faker->randomDigit()]
        ];        
        $icon = $this->faker->imageUrl;// send icon url or file
        $processType = $this->faker->randomElement(ProcessType::toArray());
        // send request      
        $response = $this->suite->application()->create($appName, $appPlatforms, $icon, $processType);
        return json_encode($response);
    } catch (\Throwable $exception) {
        if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
            http_response_code($exception->getCode());
            return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
        }
        return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
    } 
 }
```

## Send icon with presigned url

With using this feature, you are able to send file from your service instead of sending it via api. The presigned URLs
are valid only for the specified duration (+10 min).

Before doing this, you should have application id:

```php
public function uploadApplicationIcon()
{
   try {
        $response = $this->suite->application()->generatePresignedUrl($id, $name);
        $url = $response['data']['url'];
        // now you can upload file to this url via PUT method...
        // after sending file, you should update application again with new icon url
        return json_encode($response);
   } catch (\Throwable $exception) {
        if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
            http_response_code($exception->getCode());
            return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
        }
        return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
    }
 }
```

## Get application by id

```php
 public function show($id)
 {
     try {
          $response = $this->suite->application()->show($id);
          return json_encode($response);
     } catch (\Throwable $exception) {
        if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
            http_response_code($exception->getCode());
            return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
        }
        return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
    }
 }
```

## Create application

For creating an application name is enough to get id. platforms can be assigned later with update method:

```php
 use Rockads\Suite\Constants\PlatformType;

 public function update(int $id)
 {
    try {      
        $appName = $this->faker->name;
        $appPlatforms = [
          ['platform' => PlatformType::IOS, 'bundle_id' => $this->faker->randomDigit()]
        ];        
        // send request        
        $response = $this->suite->application()->update($id, $appName, $appPlatforms);
        return json_encode($response);
    } catch (\Throwable $exception) {
        if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
            http_response_code($exception->getCode());
            return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
        }
        return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
    }  
 }
```

Notice: uploading application icon can be sync or async, if you want to do as async you should send that as a 5th
parameter to create or update method.

## Delete application

```php
public function delete($id) 
{
    try {
        $response = $this->suite->application()->destroy($id);
        return json_encode($response);
    } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## Application page urls

Maybe you need to redirect user to Suite's panel to manage customer applications instead of doing same thing from your
service side:

Notice: You can set callback url for redirecting back user to your service after that action! (optional)

```php
public function getCreatePageUrl() 
{
   try {
        // application create page url
        $callbackUrl = 'https://your-domain.com/user/list';
        $responseCreate = $this->suite->application()->getCreatePageUrl($callbackUrl);
        // application list page url
        $responseList = $this->suite->application()->getListPageUrl();
        // response
        return json_encode([
            'status'=>true, 
            'message'=>'Application pages url retrieved successfully.', 
            'data'=> [
                'app_create_page_url'=> $responseCreate,
                'app_list_page_url'=> $responseList,
            ],
        ]);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```