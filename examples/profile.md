# Working with profile

Here, you can get and manipulate logged-in user information.

## Make instance of Suite class

<b>Notice:</b> If you are using <b>Laravel</b>, SuiteServiceProvider bind <b>oAuth</b> and <b>Suite</b> automatically as
a Singleton and you will not get any errors about dependency injection.

Otherwise, you should bind this two class in your framework <b>service container</b> manually or just 'new' these class
inside your methods.

* Laravel:

```php
use Rockads\Suite\Suite;
use Rockads\Suite\Models\Config;

class ProfileController {

    protected Suite $suite;

    public function __construct(Suite $suite)
    {
        // get token
        $userId = auth()->user()->id;
        $token = $this->getToken($userId);
        $suite->setToken($token);
    
        $this->suite = $suite;
    }
```

* Other frameworks or pure PHP without injecting Suite SDK dependencies:

```php
use Rockads\Suite\Suite;
use Rockads\Suite\Models\Config;
use GuzzleHttp\Client;

class ProfileController {
  
    protected Suite $suite;

    public function __construct()
    {      
        $client = new Client();
        $config = Config::make(config('suite'));   
        // get token
        $userId = auth()->user()->id;
        $token = $this->getToken($userId); 

        $this->suite = new Suite($token, $config, $client);
    }
```

Notice: User should have been authenticated in all module controllers like Application, Profile and User. (protected
routes)

So we have user id, and we can pass it to getToken method to get token which is got from oAuth procedure:

```php
protected function getToken($userId): Token
{
    if (Cache::has('token_' . $userId))
        return Cache::get('token_' . $userId);
    else
        throw new \Exception('Token is not exist');
}
```

## Get Profile

As you use this module in OAuth procedure, after getting token with calling me method, Suite return user profile and
which role and applications is assigned to user(customer-based):

```php
public function me()
{
  try {
    $meResponse = $this->suite->profile()->me();
    return json_encode($response);
  } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

For just returning user id at me method in order to verify user quickly, you need to send false as a parameter:

```php
public function verifyToken()
{
   try {
        $response = $this->suite->profile()->me(false);
        if ($response['status']) {
            throw new Exception('Token is invalid!');
        }            
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## Change password

```php
public function changePassword()
{
   try {
         $currentPassword = 'user\'s current pass';
         $newPassword = generateRandomString(8);
         $response = $this->suite->profile()->changePassword($currentPassword, $newPassword);
        return json_encode($response);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## Update profile

```php
public function updateProfile(Request $request)
{
   try {
         $data = [
            'name' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'timezone' => $this->faker->timezone,
            'language' => $this->faker->locale,
            'avatar' => $this->faker->image
        ];
        $response = $this->suite->profile()->update($data);
        return json_encode($response);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## Logout

You should notice that if you log out a user, it will be logged out of all other services, so after logout you can
redirect user to suite login page:

```php
public function logout()
{
   try {
        $response = $this->suite->profile()->logout();
        $this->clearTokenCache(auth()->user()->id);
        return json_encode($response);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## Profile page url

Maybe you need to redirect user to Suite's panel to update its profile and change password instead of doing same thing
from your service side:

Notice: You can set callback url for redirecting back user to your service after that action! (optional)

```php
public function getProfilePageUrl()
{
   try {
        $callbackUrl = 'https://your-domain.com/dashboard';
        $response = $this->suite->profile()->getProfilePageUrl($callbackUrl);
        return json_encode($response);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```