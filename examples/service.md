# Working with services

Services are assigned to users to access them from Suite's panel dashboard.

## Make instance of Suite class

<b>Notice:</b> If you are using <b>Laravel</b>, SuiteServiceProvider bind <b>oAuth</b> and <b>Suite</b> automatically as
a Singleton and you will not get any errors about dependency injection.

Otherwise, you should bind this two class in your framework <b>service container</b> manually or just 'new' these class
inside your methods.

* Laravel:

```php
use Rockads\Suite\Suite;
use Rockads\Suite\Models\Config;

class ServiceController {

    protected $suite;

    public function __construct(Suite $suite)
    {
        // get token
        $userId = auth()->user()->id;
        $token = $this->getToken($userId);
        $suite->setToken($token);
  
        $this->suite = $suite;
    }
```

* Other frameworks or pure PHP without injecting Suite SDK dependencies:

```php
use Rockads\Suite\Suite;
use Rockads\Suite\Models\Config;
use GuzzleHttp\Client;

class ServiceController {
  
    protected Suite $suite;

    public function __construct()
    {      
        $client = new Client();
        $config = Config::make(config('suite'));   
        // get token
        $userId = auth()->user()->id;
        $token = $this->getToken($userId); 

        $this->suite = new Suite($token, $config, $client);
    }
```

Notice: User should have been authenticated in all module controllers like Application, Profile and User. (protected
routes)

So we have user id, and we can pass it to getToken method to get token which is got from oAuth procedure:

```php
protected function getToken($userId): Token
{
    if (Cache::has('token_' . $userId))
        return Cache::get('token_' . $userId);
    else
        throw new \Exception('Token is not exist');
}
```

## Get services

```php
use Rockads\Suite\Constants\ResultType;

public function all()
{
  try {      
      $resultType = $this->faker->randomElement(ResultType::toArray());
      $response = $this->suite->service()->all($resultType);
      return json_encode($response);
  } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## Get a service

```php
public function getServiceById($id)
{
  try {      
      $response = $this->suite->service()->show($id);
      return json_encode($response);
  } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```