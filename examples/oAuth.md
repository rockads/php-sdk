# Explain OAuth Scenario

For integrating with this microservice firstly, you should remove your internal authentication system because Suite
handle all auth request and give a token for user.

Your service can use this token as a main token for communication with frontend, or Alternatively, you can map Suite's
token with your system's inner jwt token and use your own jwt.(not preferred)

<img src="assets/oauth.png" alt="suite login page" style="display: block;  width: 40%; margin-left: auto; margin-right: auto;"/>


## Make instance of Suite's OAuth class

<b>Notice:</b> If you are using <b>Laravel</b>, SuiteServiceProvider bind <b>oAuth</b> and <b>Suite</b> automatically as
a Singleton and you will not get any errors about dependency injection.

Otherwise, you should bind this two class in your framework <b>service container</b> manually or just 'new' these class
inside your methods.

* Laravel:

```php 
use Rockads\Suite\OAuth;
use Rockads\Suite\Models\Config;

class OAuthController {

  protected OAuth $oAuth;

  public function __construct(OAuth $oAuth)
  {      
      $this->oAuth = $oAuth;
  }
```

* Other frameworks or pure PHP without injecting Suite SDK dependencies:

```php
use Rockads\Suite\OAuth;
use Rockads\Suite\Models\Config;
use GuzzleHttp\Client;

class OAuthController {

  protected OAuth $oAuth;

  public function __construct()
  {      
      $client = new Client(); 
      $config = Config::make(config('suite'));
      $this->oAuth = new OAuth($config, $client);      
  }
```

## Get Token

1. Firstly, You should get Auth page urls from SDK (these are public endpoints) and pass it to frontend to place them on
   login & register button:

```php
public function getAuthUrls()
{
  try {
      $pageUrls['registerUrl'] = $this->oAuth->getRegisterUrl();
      $pageUrls['loginUrl'] = $this->oAuth->getLoginUrl();
      return json_encode(['status'=>true,'data'=>$pageUrls, 'message'=>'Getting auth page urls is done.']);
  } catch (\Throwable $exception) {
    if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
        http_response_code($exception->getCode());
        return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
    }
    return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
}
```
   
2. By clicking on login or register button, user will be redirected to Suite’s auth pages.


3. After successful authentication or registration, user redirect back to service's redirect url which is set in SDK's
   config to authorize service:
   
```php
 Route::get('/oauth/redirect', [oAuthController::class, 'redirect'])->name('redirect');
```
   
4. Then, service redirect again to Suite with client information getting from Suite administrator:
    
```php
public function oAuthRedirect()
{
  $state = generateRandomString(40);
  session('state', $state);
  $url = $this->oAuth->getRedirectUrl($state);
  return redirect()->away($url);
}
```
   
5. If client information is correct, Suite redirect back to service's callback url with exchange code:
   
```php
 Route::get('/oauth/callback', [oAuthController::class, 'callback'])->name('callback');
```

6. Finally, service send exchange code with client information to Suite to get token(cache it until token expire-data for feature use).
   
```php
public function oAuthCallback(Request $request)
{
  if (isset($_GET['error'])) {
      die($_GET['error']);
  }
  $exchangeCode = $_GET['code'];
  $state = $_GET['state'];
  // check state is same
  if (strlen($state) > 0 && $state !== session('state')) {
      throw new \Exception('invalid argument');
  }
  session()->forget('state');
  // get token
  $token = $this->oAuth->getTokenByExchangeCode($exchangeCode);
  // get user by token
  $me = $this->me($token);

  // optional
  $this->createOrUpdateUser($me);

  return redirect()->route('profile')->with('user', $me);
}
```
   
Notice: In some cases, services might be kept users information so, after getting user profile you should create or update the user:

```php
use App\Models\User;
use Hash;

protected function createOrUpdateUser(array $meResponse)
{
  $user = User::where('email', $meResponse['data']['email'])->first();
  if (!$user) {
      $user = new User();
      $user->id = $meResponse['data']['id'];
      $user->name = $meResponse['data']['name'];
      $user->email = $meResponse['data']['email'];
      $user->password = Hash::make(generateRandomString(8));
      $user->save();
  } else {
      // update user
      $user->update([
          'name' => $meResponse['data']['name'],
          'email' => $meResponse['data']['email'],
      ]);
  }
}
```

## Get profile

First thing after getting token is achieving to user information which is bind to this token.
(more information about **profile** is located [here](/examples/profile.md))

```php
protected function me(Token $token): array
{
  $suite = Suite::make($token, $this->config);
  $response = $suite->profile()->me();
  $response['data']['token'] = $token->toArray();
  $this->setToken($token, $user->id);
  // log user in manually in your system
  auth()->login($user);
  
  return json_encode($response);
}
```

### Caching & retrieving token

Maybe the best way make these function a trait and use in your controllers: 

```php
protected function setToken(Token $token, $userId): void
{
  $cachePeriod = $token->getExpiresIn() - strtotime('now');
  $cacheUniqueKey = 'token_' . $userId;
  Cache::put($cacheUniqueKey, $token, $cachePeriod);
}

protected function getToken($userId): Token
{
  if (Cache::has('token_' . $userId))
      return Cache::get('token_' . $userId);
  else
      throw new \Exception('Token is not exist');
}

protected function clearTokenCache($userId): void
{
  Cache::forget('token_' . $userId);
}
```

## Get refresh token

By getting refresh token you can extend the access token lifetime with old refresh token until user will be logout:

```php
public function refreshToken()
{
  try {
      $userId = auth()->user()->id;
      $token = $this->getToken($userId);// get old token from cache
      $newToken = $this->oAuth->refreshToken($token->getRefreshToken());
      $this->setToken($newToken, $userId);// override user token
      return json_encode('status'=>true, 'message'=>'Getting refresh token is done.', 'data'=>$newToken);
  } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
           http_response_code($exception->getCode());
           return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```