# Working with User & Customer

## Make instance of Suite class

<b>Notice:</b> If you are using <b>Laravel</b>, SuiteServiceProvider bind <b>oAuth</b> and <b>Suite</b> automatically as
a Singleton and you will not get any errors about dependency injection.

Otherwise, you should bind this two class in your framework <b>service container</b> manually or just 'new' these
classes inside your methods.

* Laravel:

```php
use Rockads\Suite\Suite;
use Rockads\Suite\Models\Config;

class UserController {

    protected Suite $suite;

    public function __construct(Suite $suite)
    {
        // get token    
        $userId = auth()->user()->id;
        $token = $this->getToken($userId);
        $suite->setToken($token); 

        $this->suite = $suite;
    }
```

* Other frameworks or pure PHP without injecting Suite SDK dependencies:

```php
use Rockads\Suite\Suite;
use Rockads\Suite\Models\Config;
use GuzzleHttp\Client;

class UserController {
  
    protected Suite $suite;

    public function __construct()
    {      
        $client = new Client();
        $config = Config::make(config('suite'));   
        // get token
        $userId = auth()->user()->id;
        $token = $this->getToken($userId); 

        $this->suite = new Suite($token, $config, $client);
    }
```

Notice: User should have been authenticated in all module controllers like Application, Profile and User. (protected
routes)

So we have user id, and we can pass it to getToken method to get token which is got from oAuth procedure:

```php
protected function getToken($userId): Token
{
    if (Cache::has('token_' . $userId))
        return Cache::get('token_' . $userId);
    else
        throw new \Exception('Token is not exist');
}
```

## Get all users or paginated list of users

Get users of logged-in workspace:

```php
use Rockads\Suite\Constants\ResultType;

public function users()
{
  try {      
        $resultType = $this->faker->randomElement(ResultType::toArray());
        $response = $this->suite->user()->all($resultType);
        return json_encode($response);
  } catch (\Throwable $exception) {
        if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
            http_response_code($exception->getCode());
            return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
        }
        return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  } 
}
```

## Register a customer

For registering a customer, user should be registered itself by suite's register page, but in some situations, services
need to register customer via API:

```php
public function register()
{
  try {                
      $customer = [
          'name'=> $this->faker->name,
          'workspace'=> $this->faker->name,
      ];
      $user = [
          'name'=> $this->faker->name,
          'email'=> $this->faker->email,
          'language'=> $this->faker->randomElement(['tr','en']),
      ];       
      $response = $this->suite->customer()->register($customer, $user);
      return json_encode($response);
  } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  } 
}
```

## Invite user

Invite a user for a specific customer:

```php
public function inviteUser()
{
  try {               
      $data = [
        'customer_id' => $this->faker->randomNumber(),
        'email' => $this->faker->email(),
        'language' => $this->faker->locale,            
        'services' => [
            'service_id' => $this->faker->randomNumber(),
            'role_id' => $this->faker->randomNumber(),
            'applications' => [
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
            ],
        ],
      ]);                      
      $response = $this->suite->user()->create($data);
      return json_encode($response);
  } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  } 
}
```

## Get user by id

This method's response structure like <b>me response</b> just for another users:

```php
public function getUserById($id)
{
   try {
        $response = $this->suite->user()->show($id);
        return json_encode($response);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## update user

Update specific user:

```php
public function updateUser(int $id)
{
  try {             
      $data = [
        'customer_id' => $this->faker->randomNumber(),
        'email' => $this->faker->email(),
        'language' => $this->faker->locale,            
        'name'=> $this->faker->name,
        'username'=> $this->faker->userName,
        'timezone'=> $this->faker->timezone,
        'services' => [
            'service_id' => $this->faker->randomNumber(),
            'role_id' => $this->faker->randomNumber(),
            'applications' => [
                $this->faker->randomNumber(),
            ],
        ],
      ]);
      $response = $this->suite->user()->update($id, $data);
      return json_encode($response);
  } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  } 
}
```

## Delete a user

```php
public function deleteUser($id)
{
   try {
        $response = $this->suite->user()->destroy($id);
        return json_encode($response);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

Notice: If you just avoid user to login:

```php
use Rockads\Suite\Constants\UserStatusType;

public function disableUserStatus($id)
{
   try {
        $response = $this->suite->user()->update([
            'status'=> UserStatusType::PASSIVE,
        ]);
        return json_encode($response);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```

## User page urls

Maybe you need to redirect user to Suite's panel to create or list users instead of doing same thing from your service
side:

Notice: You can set callback url for redirecting back user to your service after that action! (optional)

```php
public function getCreatePageUrl()
{
   try {
        // user create page url
        $callbackUrl = 'https://your-domain.com/dashbaord';
        $responseCreate = $this->suite->user()->getCreatePageUrl($callbackUrl);
        // user list page url
        $responseList = $this->suite->user()->getListPageUrl();
        // response
        return json_encode([
            'status'=>true, 
            'message'=>'Users pages url retrieved successfully.', 
            'data'=> [
                'user_create_page_url'=> $responseCreate,
                'user_list_page_url'=> $responseList,
            ],
        ]);
   } catch (\Throwable $exception) {
      if ($exception instanceof \Rockads\Suite\Exceptions\SuiteException) {
          http_response_code($exception->getCode());
          return json_encode(['status'=>false, 'message'=>$exception->getMessage(), 'data'=>$exception->getData()]);
      }
      return json_encode(['status'=>false, 'message'=>$exception->getMessage()]);
  }
}
```