<?php

namespace Rockads\Suite\Models;

use Rockads\Suite\Constants\TokenFields;
use Rockads\Suite\Exceptions\SuiteException;

/**
 * Class Token
 * @package Rockads\Suite\Models
 */
class Token
{
    /**
     * @var string
     */
    protected string $tokenType;

    /**
     * @var string
     */
    protected string $accessToken;

    /**
     * @var string
     */
    protected string $refreshToken;

    /**
     * @var int
     */
    protected int $expiresIn;

    /**
     * @param array $token
     *
     * @return static
     * @throws \Exception
     */
    public function __construct(array $token)
    {
        // validate
        $this->validateTokenArray($token);
        $this->setTokenType($token[TokenFields::TOKEN_TYPE]);
        $this->setAccessToken($token[TokenFields::ACCESS_TOKEN]);
        $this->setExpiresIn($token[TokenFields::EXPIRES_IN]);
        // client_credential grant does not have any refreshToken
        if (isset($token[TokenFields::REFRESH_TOKEN])) {
            $this->setRefreshToken($token[TokenFields::REFRESH_TOKEN]);
        }
        return $this;
    }

    /**
     * @param array $token
     *
     * @return void
     * @throws \Exception
     */
    private function validateTokenArray(array $token)
    {
        if (count($token) < 1 || !isset($token[TokenFields::TOKEN_TYPE]) || !isset($token[TokenFields::ACCESS_TOKEN]) || !isset($token[TokenFields::EXPIRES_IN]))
            throw new SuiteException('Token array is not valid.');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            TokenFields::TOKEN_TYPE => $this->getTokenType(),
            TokenFields::EXPIRES_IN => $this->getExpiresIn(),
            TokenFields::ACCESS_TOKEN => $this->getAccessToken(),
            TokenFields::REFRESH_TOKEN => isset($this->refreshToken) ? $this->getRefreshToken() : null,
        ];
    }

    /**
     * @param int $expiresIn
     *
     * @throws \Exception
     */
    public function setExpiresIn(int $expiresIn): void
    {
        $this->expiresIn = time() + $expiresIn;
    }

    /**
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    /**
     * @return string
     */
    public function getTokenType(): string
    {
        return $this->tokenType;
    }

    /**
     * @param string $tokenType
     */
    public function setTokenType(string $tokenType): void
    {
        $this->tokenType = $tokenType;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken(string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }
}
