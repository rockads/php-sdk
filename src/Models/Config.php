<?php

namespace Rockads\Suite\Models;

use Rockads\Suite\Constants\ApiVersionType;
use Rockads\Suite\Constants\OAuthFields;

/**
 * Class Config
 * @package Rockads\Suite\Models
 */
class Config extends Singleton
{
    /**
     * @var string
     */
    protected string $baseUrl;

    /**
     * @var string
     */
    protected string $redirectUri;

    /**
     * @var string
     */
    protected string $callbackUri;

    /**
     * @var string
     */
    protected string $apiVersion;

    /**
     * @var int
     */
    protected int $clientId;

    /**
     * @var string
     */
    protected string $clientSecret;

    /**
     * @param array $config
     *
     * @return static
     */
    public static function make(array $config): self
    {
        $obj = self::getInstance();
        $obj->setApiVersion(ApiVersionType::V1);
        $obj->setBaseUrl($config[OAuthFields::BASE_URL]);
        $obj->setClientId($config[OAuthFields::CLIENT_ID]);
        $obj->setClientSecret($config[OAuthFields::CLIENT_SECRET]);
        $obj->setRedirectUri($config[OAuthFields::OAUTH_REDIRECT_URL]);
        $obj->setCallbackUri($config[OAuthFields::OAUTH_CALLBACK_URL]);
        return $obj;
    }

    /**
     * @return string
     */
    public function getCallbackUri(): string
    {
        return $this->callbackUri;
    }

    /**
     * @param string $callbackUri
     */
    public function setCallbackUri(string $callbackUri): void
    {
        $this->callbackUri = $callbackUri;
    }

    /**
     * @return string
     */
    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    /**
     * @param string $redirectUri
     */
    public function setRedirectUri(string $redirectUri): void
    {
        $this->redirectUri = $redirectUri;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return string
     */
    public function getApiVersion(): string
    {
        return $this->apiVersion;
    }

    /**
     * @param string $apiVersion
     */
    public function setApiVersion(string $apiVersion): void
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId
     */
    public function setClientId(int $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret(string $clientSecret): void
    {
        $this->clientSecret = $clientSecret;
    }

}