<?php

namespace Rockads\Suite;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\ModulesType;
use Rockads\Suite\Exceptions\SuiteException;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\Modules\Application;
use Rockads\Suite\Modules\Customer;
use Rockads\Suite\Modules\ModuleInterface;
use Rockads\Suite\Modules\Profile;
use Rockads\Suite\Modules\Service;
use Rockads\Suite\Modules\User;

/**
 * Class Suite
 * @package Rockads\Suite
 */
class Suite
{
    /**
     * @var \Rockads\Suite\Models\Token
     */
    protected Token $token;

    /**
     * @var \Rockads\Suite\Models\Config
     */
    protected Config $config;

    /**
     * @var
     */
    protected $module;

    /**
     * @var \GuzzleHttp\Client
     */
    protected Client $client;

    /**
     * @param \Rockads\Suite\Models\Token $token
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Token $token, Config $config, Client $client)
    {
        $this->token = $token;
        $this->config = $config;
        $this->client = $client;
    }

    /**
     * @param string $moduleName
     *
     * @return \Rockads\Suite\Modules\ModuleInterface
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    protected function setModule(string $moduleName): ModuleInterface
    {
        if (!in_array($moduleName, ModulesType::toArray())) {
            throw new SuiteException('The given module name is invalid');
        }
        $class = sprintf("\Rockads\Suite\Modules\%s", $moduleName);
        $this->module = new $class($this->token, $this->config, $this->client);
        return $this->module;
    }

    //------------------------------------------------------------

    /**
     * @return \Rockads\Suite\Modules\Customer
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function customer(): Customer
    {
        return $this->setModule(ModulesType::CUSTOMER);
    }

    /**
     * @return \Rockads\Suite\Modules\User
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function user(): User
    {
        return $this->setModule(ModulesType::USER);
    }

    /**
     * @return \Rockads\Suite\Modules\Profile
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function profile(): Profile
    {
        return $this->setModule(ModulesType::PROFILE);
    }

    /**
     * @return \Rockads\Suite\Modules\Application
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function application(): Application
    {
        return $this->setModule(ModulesType::APPLICATION);
    }

    /**
     * @return \Rockads\Suite\Modules\Service
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function service(): Service
    {
        return $this->setModule(ModulesType::SERVICE);
    }

    //------------------------------------------------------------

    /**
     * @return \Rockads\Suite\Models\Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }

    /**
     * @param \Rockads\Suite\Models\Token $token
     */
    public function setToken(Token $token): void
    {
        $this->token = $token;
    }

    /**
     * @return \Rockads\Suite\Models\Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @param \Rockads\Suite\Models\Config $config
     */
    public function setConfig(Config $config): void
    {
        $this->config = $config;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }
}
