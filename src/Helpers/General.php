<?php

if (!function_exists('pathJoin')) {
    function pathJoin(string $base, string $path): string
    {
        return rtrim($base, '/') . '/' . ltrim($path, '/');
    }
}

if (!function_exists('camelToSnake')) {
    function camelToSnake($input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }
}

if (!function_exists('snakeToCamel')) {
    function snakeToCamel($input): string
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $input))));
    }
}

if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('UrlHasSchema')) {
    function UrlHasSchema($strUrl): bool
    {
        $arrParsedUrl = parse_url($strUrl);
        if (!empty($arrParsedUrl['scheme'])) {
            if ($arrParsedUrl['scheme'] == "http" || $arrParsedUrl['scheme'] == "https") {
                return true;
            }
            return false;
        }
        return false;
    }
}