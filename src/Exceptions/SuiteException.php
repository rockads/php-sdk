<?php

namespace Rockads\Suite\Exceptions;

use Exception;

/**
 * Class SuiteException
 * @package Rockads\Suite\Exceptions
 */
class SuiteException extends Exception
{
    /**
     * @var array
     */
    protected array $_data;

    /**
     * SuiteException constructor.
     *
     * @param string $message
     * @param array $data
     * @param int $exceptionCode
     */
    public function __construct(string $message, array $data = [], int $exceptionCode = 400)
    {
        $this->_data = $data;
        parent::__construct($message);
        if (400 <= $exceptionCode && $exceptionCode <= 600)
            $this->code = $exceptionCode;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->_data;
    }
}

