<?php

namespace Rockads\Suite;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Rockads\Suite\Constants\TokenFields;
use Rockads\Suite\Models\Token;

/**
 * Class SuiteServiceProvider
 * @package Rockads\Suite
 */
class SuiteServiceProvider extends ServiceProvider
{

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/suite.php', 'suite');

        $this->app->singleton(OAuth::class, function ($app) {
            $config = config('suite');
            $client = new Client();
            return new OAuth($config, $client);
        });

        $this->app->singleton(Suite::class, function ($app) {
            $config = config('suite');
            $client = new Client();
            $token = new Token([
                TokenFields::TOKEN_TYPE => 'Berear',
                TokenFields::ACCESS_TOKEN => '',
                TokenFields::EXPIRES_IN => 0,
            ]);
            return new Suite($token, $config, $client);
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['suite'];
    }
}
