<?php

namespace Rockads\Suite\GrantTypes;

use GuzzleHttp\Client;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Traits\HttpRequest;

/**
 * Class AbstractGrantType
 * @package Rockads\Suite\GrantTypes
 */
abstract class AbstractGrantType implements GrantTypeInterface
{
    use HttpRequest {
        HttpRequest::__construct as private __httpConstruct;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->__httpConstruct($client);
    }

    /**
     * @var \Rockads\Suite\Models\Config
     */
    protected Config $config;

    /**
     * @var string
     */
    protected string $url = 'oauth/token';

    /**
     * @var \GuzzleHttp\Client
     */
    protected Client $client;

    /**
     * @var array
     */
    protected array $params = [];

    // ------------------------------------

    /**
     * @return \Rockads\Suite\Models\Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @param \Rockads\Suite\Models\Config $config
     */
    public function setConfig(Config $config): void
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }
}
