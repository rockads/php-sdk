<?php

namespace Rockads\Suite\GrantTypes\Handlers;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\GrantType;
use Rockads\Suite\Constants\OAuthFields;
use Rockads\Suite\Models\Config;
use Rockads\Suite\GrantTypes\AbstractGrantType;
use Rockads\Suite\Models\Token;

/**
 * Class ClientCredentialsHandler
 * @package Rockads\Suite\GrantTypes\Handlers
 */
class ClientCredentialsHandler extends AbstractGrantType
{
    /**
     * @var string
     */
    protected string $moduleName = 'ClientCredential(grantType)';

    /**
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     * @param ...$params
     */
    public function __construct(Config $config, Client $client,...$params)
    {
        $this->config = $config;
        $this->client = $client;
        $this->params = $params;
        // trait
        parent::__construct($client);
    }

    /**
     * @param ...$params
     *
     * @return \Rockads\Suite\Models\Token
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getToken(...$params): Token
    {
        $loginParams = [
            GrantType::FIELD => GrantType::CLIENT_CREDENTIALS,
            OAuthFields::CLIENT_ID => $this->config->getClientId(),
            OAuthFields::CLIENT_SECRET => $this->config->getClientSecret(),
        ];

        $content = $this->post(
            pathJoin($this->config->getBaseUrl(), $this->url),
            $this->moduleName,
            $loginParams
        );

        return new Token($content);
    }
}
