<?php

namespace Rockads\Suite\GrantTypes\Handlers;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\GrantType;
use Rockads\Suite\Constants\OAuthFields;
use Rockads\Suite\Models\Config;
use Rockads\Suite\GrantTypes\AbstractGrantType;
use Rockads\Suite\Models\Token;

/**
 * Class PasswordGrantHandler
 * @package Rockads\Suite\GrantTypes\Handlers
 */
class PasswordGrantHandler extends AbstractGrantType
{
    /**
     * @var string
     */
    protected string $moduleName = 'PasswordGrant(grantType)';

    /**
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     * @param ...$params
     */
    public function __construct(Config $config, Client $client, ...$params)
    {
        $this->config = $config;
        $this->client = $client;
        $this->params = $params;
        // trait
        parent::__construct($client);
    }

    /**
     * @param ...$params
     *
     * @return \Rockads\Suite\Models\Token
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getToken(...$params): Token
    {
        $loginParams = [
            GrantType::FIELD => GrantType::PASSWORD_GRANT,
            OAuthFields::CLIENT_ID => $this->config->getClientId(),
            OAuthFields::CLIENT_SECRET => $this->config->getClientSecret(),
            "email" => $params[0],
            "password" => $params[1],
        ];

        $content = $this->post(
            pathJoin($this->config->getBaseUrl(), $this->url),
            $this->moduleName,
            $loginParams
        );

        return new Token($content);
    }
}
