<?php

namespace Rockads\Suite\GrantTypes;

use GuzzleHttp\Client;
use Rockads\Suite\Models\Config;

/**
 * Class GrantTypeFactory
 * @package Rockads\Suite\GrantTypes
 */
class GrantTypeFactory
{
    /**
     * @param string $type
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     * @param ...$params
     *
     * @return \Rockads\Suite\GrantTypes\GrantTypeInterface
     */
    public static function create(string $type, Config $config, Client $client, ...$params): GrantTypeInterface
    {
        $class = sprintf('\\%s\Handlers\\%sHandler', __NAMESPACE__, ucfirst(snakeToCamel($type)));
        return new $class($config, $client, $params);
    }
}
