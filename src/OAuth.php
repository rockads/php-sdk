<?php

namespace Rockads\Suite;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\GrantType;
use Rockads\Suite\Constants\OAuthFields;
use Rockads\Suite\Exceptions\SuiteException;
use Rockads\Suite\GrantTypes\GrantTypeFactory;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\Traits\HttpRequest;

/**
 * Class OAuth
 * @package Rockads\Suite\Authenticate
 */
class OAuth
{
    use HttpRequest {
        HttpRequest::__construct as private __httpConstruct;
    }

    /**
     * @var \Rockads\Suite\Models\Config
     */
    protected Config $config;

    /**
     * @var string
     */
    protected string $moduleName = 'oAuth';

    /**
     * @var \Rockads\Suite\Models\Token|null
     */
    protected ?Token $token = null;

    /**
     * @var \GuzzleHttp\Client
     */
    protected Client $client;

    /**
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Config $config, Client $client)
    {
        $this->config = $config;
        $this->client = $client;
        // trait
        $this->__httpConstruct($client);
    }

    /**
     * @param string|null $appendedData
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getLoginUrl(string $appendedData = null)
    {
        if ($this->config->getRedirectUri() == '') {
            throw new SuiteException('redirect uri should be send via config');
        }
        $route = sprintf('api/%s/url/login', $this->config->getApiVersion());
        $url = pathJoin($this->config->getBaseUrl(), $route);
        $continue = '?continue=' . $this->config->getRedirectUri();
        if ($appendedData) {
            $continue .= '&' . $appendedData;
        }
        $url = pathJoin($url, $continue);
        $redirectUrl = $this->get($url, $this->moduleName);
        return $redirectUrl['data'];
    }

    /**
     * @param string|null $appendedData
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getRegisterUrl(string $appendedData = null): string
    {
        if ($this->config->getRedirectUri() == '') {
            throw new SuiteException('redirect uri should be send via config');
        }
        $route = sprintf('api/%s/url/register', $this->config->getApiVersion());
        $url = pathJoin($this->config->getBaseUrl(), $route);
        $continue = '?continue=' . $this->config->getRedirectUri();
        if ($appendedData) {
            $continue .= '&' . $appendedData;
        }
        $url = pathJoin($url, $continue);
        $redirectUrl = $this->get($url, $this->moduleName);
        return $redirectUrl['data'];
    }

    /**
     * @param string $state
     *
     * @return string
     * @throws \Exception
     */
    public function getRedirectUrl(string $state): string
    {
        if (strlen($state) !== 40) {
            throw new SuiteException('state should be 40 random characters');
        }
        $queries = http_build_query([
            OAuthFields::RESPONSE_TYPE => OAuthFields::CODE,
            OAuthFields::STATE => $state,
            OAuthFields::CLIENT_ID => $this->config->getClientId(),
            OAuthFields::OAUTH_REDIRECT_URL => $this->config->getCallbackUri(),
        ]);
        return urldecode(pathJoin($this->config->getBaseUrl(), '/oauth/authorize?' . $queries));
    }

    /**
     * @param string $code
     *
     * @return \Rockads\Suite\Models\Token
     */
    public function getTokenByExchangeCode(string $code): Token
    {
        $grantType = GrantTypeFactory::create(GrantType::AUTHORIZATION_CODE, $this->config, $this->getClient());
        return $grantType->getToken($code);
    }

    /**
     * @param string $state
     * @param string $code_verifier
     * @param int $clientId
     * @param string $redirectUri
     *
     * @return string
     * @throws \Exception
     */
    public function getRedirectUrlPKCE(string $state, string $code_verifier, int $clientId, string $redirectUri): string
    {
        if (strlen($state) !== 40 || strlen($code_verifier) !== 128) {
            throw new SuiteException('state should be 40 & code_verifier should be 128 random characters');
        }
        $codeChallenge = strtr(rtrim(base64_encode(hash('sha256', $code_verifier, true)), '='), '+/', '-_');
        $query = http_build_query([
            OAuthFields::CLIENT_ID => $clientId,
            OAuthFields::OAUTH_REDIRECT_URL => $redirectUri,
            OAuthFields::RESPONSE_TYPE => OAuthFields::CODE,
            OAuthFields::SCOPE => '',
            OAuthFields::STATE => $state,
            OAuthFields::CODE_CHALLENGE => $codeChallenge,
            OAuthFields::CODE_CHALLENGE_METHOD => OAuthFields::CODE_CHALLENGE_METHOD_VALUE,
        ]);
        return urldecode(pathJoin($this->config->getBaseUrl(), '/oauth/authorize?' . $query));
    }

    /**
     * @param string $code
     * @param string $codeVerifier
     * @param int $clientId
     * @param string $redirectUri
     *
     * @return \Rockads\Suite\Models\Token
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getTokenByExchangeCodePKCE(string $code, string $codeVerifier, int $clientId, string $redirectUri): Token
    {
        $response = $this->post(pathJoin($this->config->getBaseUrl(), '/oauth/token'), $this->moduleName, [
            GrantType::FIELD => GrantType::AUTHORIZATION_CODE,
            OAuthFields::CLIENT_ID => $clientId,
            OAuthFields::OAUTH_REDIRECT_URL => $redirectUri,
            OAuthFields::CODE_VERIFIER => $codeVerifier,
            OAuthFields::CODE => $code,
        ]);
        return new Token($response);
    }

    /**
     * @param string $refreshToken
     *
     * @return \Rockads\Suite\Models\Token
     * @throws \Exception
     */
    public function refreshToken(string $refreshToken): Token
    {
        $grantType = GrantTypeFactory::create(GrantType::REFRESH_TOKEN, $this->config, $this->getClient());
        return $grantType->getToken($refreshToken);
    }

    //------------------------------------------------------------

    /**
     * @return \Rockads\Suite\Models\Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @param \Rockads\Suite\Models\Config $config
     */
    public function setConfig(Config $config): void
    {
        $this->config = $config;
    }

    /**
     * @return \Rockads\Suite\Models\Token|null
     */
    public function getToken(): ?Token
    {
        return $this->token;
    }

    /**
     * @param \Rockads\Suite\Models\Token|null $token
     */
    public function setToken(?Token $token): void
    {
        $this->token = $token;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }
}
