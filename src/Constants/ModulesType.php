<?php

namespace Rockads\Suite\Constants;

final class ModulesType
{
    use CustomEnums;

    const CUSTOMER = "Customer";
    const USER = "User";
    const PROFILE = "Profile";
    const APPLICATION = "Application";
    const SERVICE = "Service";
}
