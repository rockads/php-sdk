<?php

namespace Rockads\Suite\Constants;

final class TokenFields
{
    use CustomEnums;

    const TOKEN_TYPE = "token_type";
    const ACCESS_TOKEN = "access_token";
    const EXPIRES_IN = "expires_in";
    const REFRESH_TOKEN = "refresh_token";
}
