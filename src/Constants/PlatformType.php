<?php

namespace Rockads\Suite\Constants;

final class PlatformType
{
    use CustomEnums;

    const ANDROID = "android";
    const IOS = "ios";
}
