<?php

namespace Rockads\Suite\Constants;

final class ApiVersionType
{
    use CustomEnums;

    const V1 = "v1";
    const V2 = "v2";
}
