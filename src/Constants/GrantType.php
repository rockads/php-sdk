<?php

namespace Rockads\Suite\Constants;

final class GrantType
{
    use CustomEnums;

    const FIELD = "grant_type";
    const AUTHORIZATION_CODE = "authorization_code";
    const CLIENT_CREDENTIALS = "client_credentials";
    const PASSWORD_GRANT = "password_grant";
    const REFRESH_TOKEN = "refresh_token";
}
