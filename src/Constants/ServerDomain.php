<?php

namespace Rockads\Suite\Constants;

final class ServerDomain
{
    use CustomEnums;

    const STAGE = 'https://stage-suite.rockads.com';
    const PRODUCTION = 'https://id.rockads.com';
}
