<?php

namespace Rockads\Suite\Constants;

final class OAuthFields
{
    use CustomEnums;

    const BASE_URL = "base_url";
    const CLIENT_ID = "client_id";
    const CLIENT_SECRET = "client_secret";
    const OAUTH_REDIRECT_URL = "oauth_redirect_uri";
    const OAUTH_CALLBACK_URL = "oauth_callback_uri";
    const RESPONSE_TYPE = "response_type";
    const CODE = "code";
    const STATE = "state";
    const SCOPE = "scope";
    const REFRESH_TOKEN = "refresh_token";
    const CODE_VERIFIER = "code_verifier";
    const CODE_CHALLENGE = "code_challenge";
    const CODE_CHALLENGE_METHOD = "code_challenge_method";
    const CODE_CHALLENGE_METHOD_VALUE = "S256";
}
