<?php

namespace Rockads\Suite\Constants;

final class UserStatusType
{
    use CustomEnums;

    const ACTIVE = 'active';
    const PASSIVE = 'passive';
}
