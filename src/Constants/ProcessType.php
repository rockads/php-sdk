<?php

namespace Rockads\Suite\Constants;

final class ProcessType
{
    use CustomEnums;

    const SYNC = 'sync';
    const ASYNC = 'async';
}
