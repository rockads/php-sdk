<?php

namespace Rockads\Suite\Constants;

use Rockads\Suite\Exceptions\SuiteException;

final class ResultType
{
    use CustomEnums;

    const LIST = 'list';
    const PAGINATE = 'paginate';

    /**
     * @param string $resultType
     *
     * @return void
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public static function validate(string $resultType)
    {
        if (!in_array($resultType, self::toArray())){
            throw new SuiteException('result type param should be one the "list" or "paginate"');
        }
    }
}
