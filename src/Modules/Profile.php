<?php

namespace Rockads\Suite\Modules;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\ModulesType;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;

/**
 * Class Profile
 * @package Rockads\Suite\Modules
 */
class Profile extends AbstractModule
{
    /**
     * @param \Rockads\Suite\Models\Token $token
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Token $token, Config $config, Client $client)
    {
        $url = pathJoin($config->getBaseUrl(), sprintf('api/%s/auth', $config->getApiVersion()));
        parent::__construct($token, $config, $client, $url, ModulesType::PROFILE);
    }

    /**
     * @param bool $notLite
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function me(bool $notLite = true): array
    {
        $url = 'me';
        if (!$notLite) {
            $url = pathJoin($url, 'lite');
        }
        $route = sprintf('api/%s/auth/%s', $this->config->getApiVersion(), $url);
        $url = pathJoin($this->config->getBaseUrl(), $route);
        return $this->withToken($this->getAccessToken())->get($url, $this->moduleName);
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function logout()
    {
        $route = sprintf('api/%s/auth/logout', $this->config->getApiVersion());
        $url = pathJoin($this->config->getBaseUrl(), $route);
        return $this->withToken($this->getAccessToken())->post($url, $this->moduleName);
    }

    /**
     * @param $currentPassword
     * @param $password
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function changePassword($currentPassword, $password)
    {
        $route = sprintf('api/%s/auth/change-password', $this->config->getApiVersion());
        $url = pathJoin($this->config->getBaseUrl(), $route);
        return $this->withToken($this->getAccessToken())->post($url, $this->moduleName, [
            'currentPassword' => $currentPassword,
            'password' => $password,
            'passwordConfirmation' => $password
        ]);
    }

    /**
     * @param array $params
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function update(array $params)
    {
        $route = sprintf('api/%s/auth/change-password', $this->config->getApiVersion());
        $url = pathJoin($this->config->getBaseUrl(), $route);
        return $this->withToken($this->getAccessToken())->post($url, $this->moduleName, $params);
    }

    /**
     * @param string|null $continue
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getProfilePageUrl(string $continue = null): string
    {
        $url = pathJoin($this->getConfig()->getBaseUrl(), sprintf('api/%s/url/user-profile' . !is_null($continue) ? '?continue=' . $continue : null, $this->getConfig()->getApiVersion()));
        $result = $this->get($url, $this->moduleName);
        return $result['data'];
    }
}
