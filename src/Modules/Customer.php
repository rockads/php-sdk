<?php

namespace Rockads\Suite\Modules;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\GrantType;
use Rockads\Suite\Constants\ModulesType;
use Rockads\Suite\GrantTypes\GrantTypeFactory;
use Rockads\Suite\GrantTypes\GrantTypeInterface;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;

/**
 * Class Customer
 * @package Rockads\Suite\Modules
 */
class Customer extends AbstractModule
{
    /**
     * @var \Rockads\Suite\GrantTypes\GrantTypeInterface
     */
    protected GrantTypeInterface $m2mAuth;

    /**
     * @var \Rockads\Suite\Models\Token
     */
    protected Token $m2mToken;

    /**
     * @param \Rockads\Suite\Models\Token $token
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Token $token, Config $config, Client $client)
    {
        $url = pathJoin($config->getBaseUrl(), sprintf('api/%s/customers', $config->getApiVersion()));
        parent::__construct($token, $config, $client, $url, ModulesType::CUSTOMER);
        // m2m instance for register
        $this->m2mAuth = GrantTypeFactory::create(GrantType::CLIENT_CREDENTIALS, $config, $client);
    }

    /**
     * @param array $customer
     * @param array $user
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function register(array $customer, array $user)
    {
        $route = sprintf('api/%s/auth/register', $this->config->getApiVersion());
        $url = pathJoin($this->config->getBaseUrl(), $route);
        $m2mToken = $this->getM2mToken();
        return $this->withToken($m2mToken->getAccessToken())->post($url, $this->moduleName, [
                'customer' => $customer,
                'user' => $user
            ]
        );
    }

    /**
     * @return \Rockads\Suite\Models\Token
     */
    protected function getM2mToken(): Token
    {
        if ($this->token && $this->token instanceof Token) {
            return $this->token;
        } else {
            return $this->m2mAuth->getToken();
        }
    }

    /**
     * @param int $id
     * @param string|null $name
     * @param string|null $workspace
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function update(int $id, ?string $name, ?string $workspace)
    {
        return $this->put(
            pathJoin($this->url, $id),
            $this->moduleName, [
                'name' => $name,
                'workspace' => $workspace,
            ]
        );
    }
}
