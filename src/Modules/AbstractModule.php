<?php

namespace Rockads\Suite\Modules;

use GuzzleHttp\Client;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;
use Rockads\Suite\Traits\HttpRequest;

/**
 * Class AbstractModule
 * @package Rockads\Suite\Modules
 */
abstract class AbstractModule implements ModuleInterface
{
    use HttpRequest {
        HttpRequest::__construct as private __httpConstruct;
    }

    /**
     * @var \Rockads\Suite\Models\Token
     */
    protected Token $token;

    /**
     * @var \Rockads\Suite\Models\Config
     */
    protected Config $config;

    /**
     * @var \GuzzleHttp\Client
     */
    protected Client $client;

    /**
     * @var string
     */
    protected string $url;

    /**
     * @var string
     */
    protected string $moduleName;

    /**
     * @param \Rockads\Suite\Models\Token $token
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     * @param string $url
     * @param string $moduleName
     */
    public function __construct(Token $token, Config $config, Client $client, string $url, string $moduleName)
    {
        $this->token = $token;
        $this->config = $config;
        $this->client = $client;
        $this->url = $url;
        $this->moduleName = $moduleName;
        // trait
        $this->__httpConstruct($client, $token->getAccessToken());
    }

    /**
     * @return \Rockads\Suite\Models\Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }

    /**
     * @param \Rockads\Suite\Models\Token $token
     */
    public function setToken(Token $token): void
    {
        $this->token = $token;
    }

    /**
     * @return \Rockads\Suite\Models\Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @param \Rockads\Suite\Models\Config $config
     */
    public function setConfig(Config $config): void
    {
        $this->config = $config;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }
}
