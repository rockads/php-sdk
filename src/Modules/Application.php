<?php

namespace Rockads\Suite\Modules;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\ModulesType;
use Rockads\Suite\Constants\ProcessType;
use Rockads\Suite\Constants\ResultType;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;

/**
 * Class Application
 * @package Rockads\Suite\Modules
 */
class Application extends AbstractModule
{
    /**
     * @param \Rockads\Suite\Models\Token $token
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Token $token, Config $config, Client $client)
    {
        $url = pathJoin($config->getBaseUrl(), sprintf('api/%s/applications', $config->getApiVersion()));
        parent::__construct($token, $config, $client, $url, ModulesType::APPLICATION);
    }

    /**
     * @param string $resultType
     * @param array $filter
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function all(string $resultType, array $filter = [])
    {
        // validate result type
        ResultType::validate($resultType);
        // make url
        $url = pathJoin($this->url, $resultType);
        if (count($filter))
            $url .= '?' . http_build_query($filter);
        // send request
        return $this->get($url, $this->moduleName);
    }

    /**
     * @param string $name
     * @param array $platforms
     * @param $icon
     * @param string $processType
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function create(string $name, array $platforms = [], $icon = null, string $processType = ProcessType::SYNC)
    {
        return $this->post(
            $this->url,
            $this->moduleName, [
                'name' => $name,
                'platforms' => $platforms,
                'process_type' => $processType,
                'icon' => $icon
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function show(int $id)
    {
        return $this->get(pathJoin($this->url, $id), $this->moduleName);
    }

    /**
     * @param int $id
     * @param string $name
     * @param array $platforms
     * @param $icon
     * @param string $processType
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function update(int $id, string $name, array $platforms = [], $icon = null, string $processType = ProcessType::SYNC)
    {
        return $this->put(
            pathJoin($this->url, $id),
            $this->moduleName, [
                'name' => $name,
                'platforms' => $platforms,
                'process_type' => $processType,
                'icon' => $icon
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function destroy(int $id)
    {
        return $this->delete(pathJoin($this->url, $id), $this->moduleName);
    }

    /**
     * @param int $id
     * @param string $name
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function generatePresignedUrl(int $id, string $name): string
    {
        $url = pathJoin($this->url, sprintf('generate-temporary-upload-url/%s', $id));
        $result = $this->post(
            $url,
            $this->moduleName, [
                'name' => $name,
            ]
        );

        return $result['data']['url'];
    }

    /**
     * @param string|null $continue
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getCreatePageUrl(string $continue = null): string
    {
        $url = pathJoin($this->getConfig()->getBaseUrl(), sprintf('api/%s/url/application-create' . !is_null($continue) ? '?continue=' . $continue : null, $this->getConfig()->getApiVersion()));
        $result = $this->get($url, $this->moduleName);
        return $result['data'];
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function getListPageUrl(): string
    {
        $url = pathJoin($this->getConfig()->getBaseUrl(), sprintf('api/%s/url/application-list', $this->getConfig()->getApiVersion()));
        $result = $this->get($url, $this->moduleName);
        return $result['data'];
    }

}
