<?php

namespace Rockads\Suite\Modules;

use GuzzleHttp\Client;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;

/**
 * Interface ModuleInterface
 * @package Rockads\Suite\Modules
 */
interface ModuleInterface
{
    public function getToken(): Token;

    public function getConfig(): Config;

    public function getClient(): Client;

    public function getUrl(): string;
}
