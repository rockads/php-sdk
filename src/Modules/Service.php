<?php

namespace Rockads\Suite\Modules;

use GuzzleHttp\Client;
use Rockads\Suite\Constants\ModulesType;
use Rockads\Suite\Constants\ResultType;
use Rockads\Suite\Models\Config;
use Rockads\Suite\Models\Token;

/**
 * Class Service
 * @package Rockads\Suite\Modules
 */
class Service extends AbstractModule
{
    /**
     * @param \Rockads\Suite\Models\Token $token
     * @param \Rockads\Suite\Models\Config $config
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Token $token, Config $config, Client $client)
    {
        $url = pathJoin($config->getBaseUrl(), sprintf('api/%s/services', $config->getApiVersion()));
        parent::__construct($token, $config, $client, $url, ModulesType::USER);
    }

    /**
     * @param string $resultType
     * @param array $filter
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \ReflectionException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function all(string $resultType, array $filter = [])
    {
        // validate result type
        ResultType::validate($resultType);
        // make url
        $url = pathJoin($this->url, $resultType);
        if (count($filter))
            $url .= '?' . http_build_query($filter);
        // send request
        return $this->get($url, $this->moduleName);
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function show(int $id)
    {
        return $this->get(pathJoin($this->url, $id), $this->moduleName);
    }
}
