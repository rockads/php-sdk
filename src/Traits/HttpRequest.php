<?php

namespace Rockads\Suite\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\UploadedFile;
use Rockads\Suite\Exceptions\SuiteException;

trait HttpRequest
{
    /**
     * @var string
     */
    protected string $accessToken;

    /**
     * @var \GuzzleHttp\Client
     */
    protected Client $client;

    /**
     * @param \GuzzleHttp\Client $client
     * @param string $accessToken
     */
    public function __construct(Client $client, string $accessToken = '')
    {
        $this->client = $client;
        $this->accessToken = $accessToken;
    }

    /**
     * @param string $accessToken
     *
     * @return $this
     */
    public function withToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $url
     * @param string $moduleName
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function get(string $url, string $moduleName)
    {
        // header
        $options = ['headers' => ['Accept' => 'application/json']];
        if (!empty($this->getAccessToken())) {
            $options['headers']['Authorization'] = 'Bearer ' . $this->getAccessToken();
        }
        // request
        $response = $this->client->get($url, $options);
        $content = json_decode($response->getBody()->getContents(), true);
        // check response code
        if ($response->getStatusCode() == 200) {
            return $content;
        } else {
            $message = is_array($content['message']) ? "Error in calling GET in $moduleName module" : $content['message'];
            throw new SuiteException($message, $content, $response->getStatusCode());
        }
    }

    /**
     * @param string $url
     * @param string $moduleName
     * @param array $data
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function post(string $url, string $moduleName, array $data = [])
    {
        // header
        $options = ['multipart' => $this->flatten($data), 'headers' => ['Accept' => 'application/json']];
        if (!empty($this->getAccessToken())) {
            $options['headers']['Authorization'] = 'Bearer ' . $this->getAccessToken();
        }
        // request
        $response = $this->client->post($url, $options);
        $content = json_decode($response->getBody()->getContents(), true);
        // check response code
        if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
            return $content;
        } else {
            $message = is_array($content['message']) ? "Error in calling POST in $moduleName module" : $content['message'];
            throw new SuiteException($message, $content, $response->getStatusCode());
        }
    }

    /**
     * @param string $url
     * @param string $moduleName
     * @param array $data
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function put(string $url, string $moduleName, array $data)
    {
        // header
        $options = ['multipart' => $this->flatten($data), 'headers' => ['Accept' => 'application/json']];
        if (!empty($this->getAccessToken())) {
            $options['headers']['Authorization'] = 'Bearer ' . $this->getAccessToken();
        }
        // request
        $response = $this->client->post($url . '?_method=PUT', $options);
        $content = json_decode($response->getBody()->getContents(), true);
        // check response code
        if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
            return $content;
        } else {
            $message = is_array($content['message']) ? "Error in calling PUT in $moduleName module" : $content['message'];
            throw new SuiteException($message, $content, $response->getStatusCode());
        }
    }

    /**
     * @param string $url
     * @param string $moduleName
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Rockads\Suite\Exceptions\SuiteException
     */
    public function delete(string $url, string $moduleName)
    {
        // header
        $options = ['headers' => ['Accept' => 'application/json']];
        if (!empty($this->getAccessToken())) {
            $options['headers']['Authorization'] = 'Bearer ' . $this->getAccessToken();
        }
        // request
        $response = $this->client->delete($url, $options);
        $content = json_decode($response->getBody()->getContents(), true);
        // check response code
        if ($response->getStatusCode() == 200) {
            return $content;
        } else {
            $message = is_array($content['message']) ? "Error in calling DELETE in $moduleName module" : $content['message'];
            throw new SuiteException($message, $content, $response->getStatusCode());
        }
    }

    /**
     * @param array $array
     * @param string $prefix
     * @param string $suffix
     *
     * @return array
     */
    private function flatten(array $array, string $prefix = '', string $suffix = ''): array
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->flatten($value, $prefix . $key . $suffix . '[', ']'));
            } else {
                if ($value instanceof UploadedFile) {
                    $result[] = [
                        'name' => $prefix . $key . $suffix,
                        'filename' => $value->getClientOriginalName(),
                        'Mime-Type' => $value->getClientMimeType(),
                        'contents' => file_get_contents($value->getPathname()),
                    ];
                } else {
                    $result[] = [
                        'name' => $prefix . $key . $suffix,
                        'contents' => $value,
                    ];
                }
            }
        }
        return $result;
    }
}
