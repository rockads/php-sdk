# Release Notes

All notable changes to `Suite's SDK` will be documented in this file.

## [v2.1](https://bitbucket.org/rockads/php-sdk/commits/tag/2.1.0) - 2022-04-22
### Added
- add service module
- add service tests
- add service documentations


## [v2.0](https://bitbucket.org/rockads/php-sdk/commits/tag/2.0.0) - 2022-04-21

### Added
- add all grant types handler

### Changed
- update all tests
- update ReadMe and documentations
- make config class as a singleton
- use constants inside modules
- get http client as a parameter for main classes

### Fixed
- fix modules send request
- fix exception


## [v1.3](https://bitbucket.org/rockads/php-sdk/commits/tag/1.3.0) - 2022-01-19

### Added
- add lite version of get profile method
- add user & application page urls

### Fixed
- fix application update and guzzle image upload
- fix return type


## [v1.2](https://bitbucket.org/rockads/php-sdk/commits/tag/1.2.0) - 2021-09-20

### Added
- add OAuth class
- add redirect & callback url to config
- add oAuth methods
- add get auth page urls methods
- add application search
- add should reset password to create user method

### Changed
- refactor SDK
- update documentations
- remove legacy auth

### Fixed
- fix url schema


## [v1.1](https://bitbucket.org/rockads/php-sdk/commits/tag/1.1.0) - 2021-08-26

### Added
- inject config to serviceProvider
- make upload application logo as async or sync
- add assign application to user method

### Changed
- update documentations

### Fixed
- fix token class
- fix config file


## [v1.0](https://bitbucket.org/rockads/php-sdk/commits/tag/1.0.0) - 2021-08-24

### Added
- create package structure 
- write base classed and modules
- add examples of use
- add simple documentation